﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generator
{
    static class Date
    {
        private static Random randomObject;

        static Date()
        {
            randomObject = new Random(Guid.NewGuid().GetHashCode());
        }

        // isn't parallel safe
        private static DateTime GenerateDate(int yearFrom, int yearTo)
        {
            return GenerateDate(new DateTime(yearFrom, 1, 1), new DateTime(yearTo, 1, 1));
        }

        private static DateTime GenerateDate(DateTime dateFrom, int yearTo)
        {
            return GenerateDate(dateFrom, new DateTime(yearTo, 1, 1));
        }

        private static DateTime GenerateDate(DateTime dateFrom, DateTime dateTo)
        {
            return GenerateDate(dateFrom, dateTo, randomObject);
        }

        // paraller safe methods
        public static DateTime GenerateDate(DateTime dateFrom, DateTime dateTo, Random rnd)
        {
            var days = (dateTo - dateFrom).Days;
            int randomDays;
            int randomMinutes;
            int randomHours;
            DateTime returnedDate = dateFrom;

            lock (randomObject) // TODO, check if it is needed
            {
                randomDays = randomObject.Next(days);
                randomMinutes= randomObject.Next(days)%60; //unused
                randomHours = randomObject.Next(7,17);
            }
            
            returnedDate = returnedDate.AddDays(randomDays);
            returnedDate = returnedDate.AddMinutes(0);
            returnedDate = returnedDate.AddHours(randomHours);

            return returnedDate; 
        }

        public static DateTime GenerateDate(int yearFrom, int yearTo, Random rnd)
        {
            return GenerateDate(new DateTime(yearFrom, 1, 1), new DateTime(yearTo, 1, 1), rnd);
        }

        public static DateTime GenerateDate(DateTime dateFrom, int yearTo, Random rnd)
        {
            return GenerateDate(dateFrom, new DateTime(yearTo, 1, 1), rnd);
        }

        public static int YearDifference(DateTime previous, DateTime later)
        {
            DateTime zeroTime = new DateTime(1, 1, 1);

            TimeSpan span = later - previous;
            // Because we start at year 1 for the Gregorian
            // calendar, we must subtract a year here.
            int years = (zeroTime + span).Year - 1;
            return years;
        }
    }
}
