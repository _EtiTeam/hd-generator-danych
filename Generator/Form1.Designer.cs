﻿namespace Generator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.generateButton = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label2 = new System.Windows.Forms.Label();
            this.localizeButton = new System.Windows.Forms.Button();
            this.Time1Box = new System.Windows.Forms.DateTimePicker();
            this.StudentBox = new System.Windows.Forms.NumericUpDown();
            this.boolTime1 = new System.Windows.Forms.CheckBox();
            this.boolTime2 = new System.Windows.Forms.CheckBox();
            this.Time2Box = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.LectorsBox = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.PresenceBox = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.ParticipationsBox = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.CoursesBox = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.ActivitiesBox = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.ExamsBox = new System.Windows.Forms.NumericUpDown();
            this.ResetButton = new System.Windows.Forms.Button();
            this.pathBox = new System.Windows.Forms.TextBox();
            this.settingsButton1 = new System.Windows.Forms.Button();
            this.settingsButton2 = new System.Windows.Forms.Button();
            this.settingsButton3 = new System.Windows.Forms.Button();
            this.settingsButton4 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.time2IncreaseDataPercentage = new Generator.PercentageNumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.StudentBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LectorsBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PresenceBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ParticipationsBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CoursesBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActivitiesBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExamsBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.time2IncreaseDataPercentage)).BeginInit();
            this.SuspendLayout();
            // 
            // generateButton
            // 
            this.generateButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.generateButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.generateButton.Location = new System.Drawing.Point(351, 476);
            this.generateButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.generateButton.Name = "generateButton";
            this.generateButton.Size = new System.Drawing.Size(324, 80);
            this.generateButton.TabIndex = 1;
            this.generateButton.Text = "Generuj";
            this.generateButton.UseVisualStyleBackColor = true;
            this.generateButton.Click += new System.EventHandler(this.generateButton_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(0, 571);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(1019, 23);
            this.progressBar1.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(48, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 32);
            this.label2.TabIndex = 5;
            this.label2.Text = "TIME 1";
            // 
            // localizeButton
            // 
            this.localizeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.localizeButton.Location = new System.Drawing.Point(67, 434);
            this.localizeButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.localizeButton.Name = "localizeButton";
            this.localizeButton.Size = new System.Drawing.Size(260, 65);
            this.localizeButton.TabIndex = 10;
            this.localizeButton.Text = "Lokalizacja";
            this.localizeButton.UseVisualStyleBackColor = true;
            this.localizeButton.Click += new System.EventHandler(this.localizeButton_Click);
            // 
            // Time1Box
            // 
            this.Time1Box.CalendarForeColor = System.Drawing.Color.Black;
            this.Time1Box.CalendarTitleForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.Time1Box.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Time1Box.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Time1Box.Location = new System.Drawing.Point(168, 18);
            this.Time1Box.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Time1Box.MaxDate = new System.DateTime(2100, 12, 31, 0, 0, 0, 0);
            this.Time1Box.MinDate = new System.DateTime(2010, 1, 1, 0, 0, 0, 0);
            this.Time1Box.Name = "Time1Box";
            this.Time1Box.Size = new System.Drawing.Size(277, 49);
            this.Time1Box.TabIndex = 13;
            this.Time1Box.Value = new System.DateTime(2016, 12, 15, 16, 35, 11, 0);
            this.Time1Box.ValueChanged += new System.EventHandler(this.Time1Box_ValueChanged);
            // 
            // StudentBox
            // 
            this.StudentBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.StudentBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.StudentBox.Location = new System.Drawing.Point(266, 79);
            this.StudentBox.Margin = new System.Windows.Forms.Padding(4);
            this.StudentBox.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.StudentBox.Name = "StudentBox";
            this.StudentBox.Size = new System.Drawing.Size(173, 41);
            this.StudentBox.TabIndex = 16;
            this.StudentBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.StudentBox.ThousandsSeparator = true;
            this.StudentBox.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.StudentBox.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // boolTime1
            // 
            this.boolTime1.AutoSize = true;
            this.boolTime1.Checked = true;
            this.boolTime1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.boolTime1.Location = new System.Drawing.Point(461, 37);
            this.boolTime1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.boolTime1.Name = "boolTime1";
            this.boolTime1.Size = new System.Drawing.Size(18, 17);
            this.boolTime1.TabIndex = 17;
            this.boolTime1.UseVisualStyleBackColor = true;
            this.boolTime1.CheckedChanged += new System.EventHandler(this.boolTime1_CheckedChanged);
            // 
            // boolTime2
            // 
            this.boolTime2.AutoSize = true;
            this.boolTime2.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.boolTime2.Location = new System.Drawing.Point(933, 37);
            this.boolTime2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.boolTime2.Name = "boolTime2";
            this.boolTime2.Size = new System.Drawing.Size(18, 17);
            this.boolTime2.TabIndex = 18;
            this.boolTime2.UseVisualStyleBackColor = true;
            this.boolTime2.CheckedChanged += new System.EventHandler(this.boolTime2_CheckedChanged);
            // 
            // Time2Box
            // 
            this.Time2Box.CalendarTrailingForeColor = System.Drawing.Color.Gray;
            this.Time2Box.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Time2Box.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.Time2Box.Location = new System.Drawing.Point(651, 15);
            this.Time2Box.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Time2Box.MaxDate = new System.DateTime(2100, 12, 31, 0, 0, 0, 0);
            this.Time2Box.MinDate = new System.DateTime(2010, 1, 1, 0, 0, 0, 0);
            this.Time2Box.Name = "Time2Box";
            this.Time2Box.Size = new System.Drawing.Size(268, 49);
            this.Time2Box.TabIndex = 19;
            this.Time2Box.ValueChanged += new System.EventHandler(this.Time2Box_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(528, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 32);
            this.label5.TabIndex = 20;
            this.label5.Text = "TIME 2";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(156, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 25);
            this.label1.TabIndex = 21;
            this.label1.Text = "Students";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(170, 169);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 25);
            this.label3.TabIndex = 23;
            this.label3.Text = "Lectors";
            // 
            // LectorsBox
            // 
            this.LectorsBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LectorsBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LectorsBox.Location = new System.Drawing.Point(266, 153);
            this.LectorsBox.Margin = new System.Windows.Forms.Padding(4);
            this.LectorsBox.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.LectorsBox.Name = "LectorsBox";
            this.LectorsBox.Size = new System.Drawing.Size(173, 41);
            this.LectorsBox.TabIndex = 22;
            this.LectorsBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.LectorsBox.ThousandsSeparator = true;
            this.LectorsBox.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.LectorsBox.ValueChanged += new System.EventHandler(this.LectorsBox_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(565, 169);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(95, 25);
            this.label4.TabIndex = 27;
            this.label4.Text = "Presence";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // PresenceBox
            // 
            this.PresenceBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.PresenceBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.PresenceBox.Location = new System.Drawing.Point(678, 153);
            this.PresenceBox.Margin = new System.Windows.Forms.Padding(4);
            this.PresenceBox.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.PresenceBox.Name = "PresenceBox";
            this.PresenceBox.Size = new System.Drawing.Size(183, 41);
            this.PresenceBox.TabIndex = 26;
            this.PresenceBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.PresenceBox.ThousandsSeparator = true;
            this.PresenceBox.Value = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.PresenceBox.ValueChanged += new System.EventHandler(this.PresenceBox_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label6.Location = new System.Drawing.Point(536, 95);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(124, 25);
            this.label6.TabIndex = 25;
            this.label6.Text = "Participatons";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // ParticipationsBox
            // 
            this.ParticipationsBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ParticipationsBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ParticipationsBox.Location = new System.Drawing.Point(678, 79);
            this.ParticipationsBox.Margin = new System.Windows.Forms.Padding(4);
            this.ParticipationsBox.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.ParticipationsBox.Name = "ParticipationsBox";
            this.ParticipationsBox.Size = new System.Drawing.Size(183, 41);
            this.ParticipationsBox.TabIndex = 24;
            this.ParticipationsBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.ParticipationsBox.ThousandsSeparator = true;
            this.ParticipationsBox.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.ParticipationsBox.ValueChanged += new System.EventHandler(this.ParticipationsBox_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label7.Location = new System.Drawing.Point(574, 236);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 25);
            this.label7.TabIndex = 31;
            this.label7.Text = "Courses";
            // 
            // CoursesBox
            // 
            this.CoursesBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CoursesBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.CoursesBox.Location = new System.Drawing.Point(678, 222);
            this.CoursesBox.Margin = new System.Windows.Forms.Padding(4);
            this.CoursesBox.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.CoursesBox.Name = "CoursesBox";
            this.CoursesBox.Size = new System.Drawing.Size(183, 41);
            this.CoursesBox.TabIndex = 30;
            this.CoursesBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.CoursesBox.ThousandsSeparator = true;
            this.CoursesBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.CoursesBox.ValueChanged += new System.EventHandler(this.CoursesBox_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label8.Location = new System.Drawing.Point(160, 238);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(86, 25);
            this.label8.TabIndex = 29;
            this.label8.Text = "Lessons";
            // 
            // ActivitiesBox
            // 
            this.ActivitiesBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ActivitiesBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ActivitiesBox.Location = new System.Drawing.Point(266, 222);
            this.ActivitiesBox.Margin = new System.Windows.Forms.Padding(4);
            this.ActivitiesBox.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.ActivitiesBox.Name = "ActivitiesBox";
            this.ActivitiesBox.Size = new System.Drawing.Size(173, 41);
            this.ActivitiesBox.TabIndex = 28;
            this.ActivitiesBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.ActivitiesBox.ThousandsSeparator = true;
            this.ActivitiesBox.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.ActivitiesBox.ValueChanged += new System.EventHandler(this.ActivitiesBox_ValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label9.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label9.Location = new System.Drawing.Point(390, 302);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 25);
            this.label9.TabIndex = 33;
            this.label9.Text = "Exams";
            // 
            // ExamsBox
            // 
            this.ExamsBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ExamsBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ExamsBox.Location = new System.Drawing.Point(485, 286);
            this.ExamsBox.Margin = new System.Windows.Forms.Padding(4);
            this.ExamsBox.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.ExamsBox.Name = "ExamsBox";
            this.ExamsBox.Size = new System.Drawing.Size(157, 41);
            this.ExamsBox.TabIndex = 32;
            this.ExamsBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.ExamsBox.Value = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.ExamsBox.ValueChanged += new System.EventHandler(this.ExamsBox_ValueChanged);
            // 
            // ResetButton
            // 
            this.ResetButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ResetButton.Location = new System.Drawing.Point(701, 439);
            this.ResetButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ResetButton.Name = "ResetButton";
            this.ResetButton.Size = new System.Drawing.Size(251, 65);
            this.ResetButton.TabIndex = 34;
            this.ResetButton.Text = "Reset";
            this.ResetButton.UseVisualStyleBackColor = true;
            this.ResetButton.Click += new System.EventHandler(this.ResetButton_Click);
            // 
            // pathBox
            // 
            this.pathBox.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pathBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pathBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pathBox.Location = new System.Drawing.Point(67, 400);
            this.pathBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pathBox.Name = "pathBox";
            this.pathBox.ReadOnly = true;
            this.pathBox.Size = new System.Drawing.Size(260, 22);
            this.pathBox.TabIndex = 35;
            this.pathBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.pathBox.TextChanged += new System.EventHandler(this.pathBox_TextChanged);
            // 
            // settingsButton1
            // 
            this.settingsButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.settingsButton1.Location = new System.Drawing.Point(461, 421);
            this.settingsButton1.Name = "settingsButton1";
            this.settingsButton1.Size = new System.Drawing.Size(49, 49);
            this.settingsButton1.TabIndex = 36;
            this.settingsButton1.Text = "1";
            this.settingsButton1.UseVisualStyleBackColor = true;
            this.settingsButton1.Click += new System.EventHandler(this.settingsButton1_Click);
            // 
            // settingsButton2
            // 
            this.settingsButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.settingsButton2.Location = new System.Drawing.Point(516, 422);
            this.settingsButton2.Name = "settingsButton2";
            this.settingsButton2.Size = new System.Drawing.Size(49, 49);
            this.settingsButton2.TabIndex = 37;
            this.settingsButton2.Text = "2";
            this.settingsButton2.UseVisualStyleBackColor = true;
            this.settingsButton2.Click += new System.EventHandler(this.settingsButton2_Click);
            // 
            // settingsButton3
            // 
            this.settingsButton3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.settingsButton3.Location = new System.Drawing.Point(571, 422);
            this.settingsButton3.Name = "settingsButton3";
            this.settingsButton3.Size = new System.Drawing.Size(49, 49);
            this.settingsButton3.TabIndex = 38;
            this.settingsButton3.Text = "3";
            this.settingsButton3.UseVisualStyleBackColor = true;
            this.settingsButton3.Click += new System.EventHandler(this.settingsButton3_Click);
            // 
            // settingsButton4
            // 
            this.settingsButton4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.settingsButton4.Location = new System.Drawing.Point(626, 422);
            this.settingsButton4.Name = "settingsButton4";
            this.settingsButton4.Size = new System.Drawing.Size(49, 49);
            this.settingsButton4.TabIndex = 39;
            this.settingsButton4.Text = "4";
            this.settingsButton4.UseVisualStyleBackColor = true;
            this.settingsButton4.Click += new System.EventHandler(this.settingsButton4_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label10.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label10.Location = new System.Drawing.Point(358, 434);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 25);
            this.label10.TabIndex = 42;
            this.label10.Text = "Preset";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label11.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label11.Location = new System.Drawing.Point(358, 370);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(162, 25);
            this.label11.TabIndex = 43;
            this.label11.Text = "2nd gen increase";
            // 
            // time2IncreaseDataPercentage
            // 
            this.time2IncreaseDataPercentage.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.time2IncreaseDataPercentage.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.time2IncreaseDataPercentage.Location = new System.Drawing.Point(541, 359);
            this.time2IncreaseDataPercentage.Margin = new System.Windows.Forms.Padding(4);
            this.time2IncreaseDataPercentage.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.time2IncreaseDataPercentage.Name = "time2IncreaseDataPercentage";
            this.time2IncreaseDataPercentage.Size = new System.Drawing.Size(134, 41);
            this.time2IncreaseDataPercentage.TabIndex = 41;
            this.time2IncreaseDataPercentage.Tag = "";
            this.time2IncreaseDataPercentage.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.time2IncreaseDataPercentage.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.time2IncreaseDataPercentage.ValueChanged += new System.EventHandler(this.time2IncreaseDataPercentage_ValueChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Generator.Properties.Resources.bg2;
            this.ClientSize = new System.Drawing.Size(1014, 605);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.time2IncreaseDataPercentage);
            this.Controls.Add(this.settingsButton4);
            this.Controls.Add(this.settingsButton3);
            this.Controls.Add(this.settingsButton2);
            this.Controls.Add(this.settingsButton1);
            this.Controls.Add(this.pathBox);
            this.Controls.Add(this.ResetButton);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.ExamsBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.CoursesBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.ActivitiesBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.PresenceBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.ParticipationsBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.LectorsBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Time2Box);
            this.Controls.Add(this.boolTime2);
            this.Controls.Add(this.boolTime1);
            this.Controls.Add(this.StudentBox);
            this.Controls.Add(this.Time1Box);
            this.Controls.Add(this.localizeButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.generateButton);
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.StudentBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LectorsBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PresenceBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ParticipationsBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CoursesBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActivitiesBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ExamsBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.time2IncreaseDataPercentage)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button generateButton;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button localizeButton;
        private System.Windows.Forms.DateTimePicker Time1Box;
        private System.Windows.Forms.NumericUpDown StudentBox;
        private System.Windows.Forms.CheckBox boolTime1;
        private System.Windows.Forms.CheckBox boolTime2;
        private System.Windows.Forms.DateTimePicker Time2Box;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown LectorsBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown PresenceBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown ParticipationsBox;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown CoursesBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown ActivitiesBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown ExamsBox;
        private System.Windows.Forms.Button ResetButton;
        private System.Windows.Forms.TextBox pathBox;
        private System.Windows.Forms.Button settingsButton1;
        private System.Windows.Forms.Button settingsButton2;
        private System.Windows.Forms.Button settingsButton3;
        private System.Windows.Forms.Button settingsButton4;
        private PercentageNumericUpDown time2IncreaseDataPercentage;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
    }
}

