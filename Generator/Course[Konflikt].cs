﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Generator
{
    class Course
    {
        public int ID_course { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int ID_exam { get; set; }

        public static void Generate(List<Course> courseList, List<Exam> examList, int count)
        {
            var randomObject = new Random(Guid.NewGuid().GetHashCode());
            for (int i = 0; i < count; i++)
            {
                Course NewCourse = new Course();
                NewCourse.ID_course = i;
                NewCourse.ID_exam = randomObject.Next(examList.Count);
                DateTime ExamDate = examList.ElementAt(NewCourse.ID_exam).Date;
                //StartDate - variable based on specified time range before ExamDate
                NewCourse.StartDate = Generator.Date.GenerateDate(ExamDate.AddYears(-1), ExamDate.AddMonths(-3));
                NewCourse.EndDate = Generator.Date.GenerateDate(ExamDate.AddDays(-10), ExamDate);
                
                courseList.Add(NewCourse);
            }
        }
        //public static void Generate2(List<Course> courseList, List<Exam> examList, DateTime lastGeneratedEnd, int count)
        //{
        //    var randomObject = new Random(Guid.NewGuid().GetHashCode());
        //    for (int i = 0; i < count; i++)
        //    {
        //        Course NewCourse = new Course();
        //        NewCourse.ID_course = i;
        //        NewCourse.ID_exam = randomObject.Next(examList.Count);
        //        DateTime ExamDate = examList.ElementAt(NewCourse.ID_exam).Date;
        //        //StartDate - variable based on specified time range before ExamDate
        //        NewCourse.StartDate = Generator.Date.GenerateDate(ExamDate.AddYears(-1), ExamDate.AddMonths(-3));
        //        NewCourse.EndDate = Generator.Date.GenerateDate(ExamDate.AddDays(-10), ExamDate);

        //        courseList.Add(NewCourse);
        //    }
        //}

        static public void saveToFile(List<Course> CourseList, char separator, string path)
        {
            string[] strArray = new string[CourseList.Count];
            for (int i = 0; i < CourseList.Count; i++)
            {
                strArray[i] = CourseList.ElementAt(i).ID_course.ToString()
                              + separator
                              + CourseList.ElementAt(i).StartDate.ToShortDateString()
                              + separator
                              + CourseList.ElementAt(i).EndDate.ToShortDateString()
                              + separator
                              + CourseList.ElementAt(i).ID_exam.ToString();
            }
            writeToFile(strArray, path);
        }

        static public void writeToFile(string[] strArray, string path)
        {
            var encoding = System.Text.Encoding.GetEncoding("ISO-8859-2");
            using (StreamWriter outputFile = new StreamWriter(path, false))
            {
                foreach (var element in strArray)
                {
                    outputFile.WriteLine(element, encoding);
                }
            }
            Cursor.Current = Cursors.Default;
        }
    }
}
