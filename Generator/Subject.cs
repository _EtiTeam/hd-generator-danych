﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generator
{
    class Subject
    {
        private static String[] subjectArray;
        private int selectedString; // index

        static Subject()
        {
            var encoding = System.Text.Encoding.GetEncoding("ISO-8859-2");

            // reading name and surnames from file
            subjectArray = File.ReadAllLines("titles.txt", encoding);
        }

        public Subject(Random rnd)
        {
            selectedString = rnd.Next(subjectArray.Count());
        }

        public string getSubject()
        {
            return subjectArray[selectedString];
        }
    }
}
