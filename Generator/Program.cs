﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Generator
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Form Generator = new Form1();
            Generator.FormBorderStyle = FormBorderStyle.FixedSingle;
            Generator.MaximizeBox = false;
            Application.Run(Generator);


        }
    }
}
