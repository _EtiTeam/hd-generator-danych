﻿using System;

using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Generator
{
    class Participation
    {
        public int ID_participation { get; set; }
        public char? Mark;
        public int Active; //check if student participates in course
        public string PESEL_student;
        public int ID_course;
        public int ID_group;

        public char? getMark()
        {
            return Mark;
        }
        public void setMark(char? mark)
        {   //mark input validation
            if (mark == 'A' || mark == 'B' || mark == 'C' || mark == 'D' || mark == null)
                Mark = mark;
        }
        public static void Generate(List<Participation> participationList, List<Course> courseList, 
            ConcurrentDictionary<string, Person> studentsList, int count, DateTime date)
        {
           var studentPesels = studentsList.Keys.ToArray();
           var concurrentBag = new ConcurrentBag<Participation>();
           Parallel.For(0, count, i =>
            {
                Participation NewParticipation = new Participation();
                NewParticipation.PESEL_student = PESELRandomGenerator(studentPesels);
                NewParticipation.ID_course = courseIdRandomGenerator(courseList);
                NewParticipation.ID_participation = i;
                // var examID = courseList[NewParticipation.ID_course].ID_exam;
                NewParticipation.Mark = markRandomGenerator(courseList, NewParticipation.ID_course, date);
                NewParticipation.Active = activeRandomGenerator(NewParticipation.Mark);

                NewParticipation.ID_group = groupRandomGenerator(count / 5);
                concurrentBag.Add(NewParticipation);
            });
           participationList.AddRange(concurrentBag);
        }

        private static char? markRandomGenerator(List<Course> courseList, int idCourse, DateTime date)
        {
            if (courseList[idCourse].EndDate < date)
            {
                var randomObject = new Random(Guid.NewGuid().GetHashCode());
                switch (randomObject.Next(4) + 1)
                {
                    case 1:
                        return 'A';
                    case 2:
                        return 'B';
                    case 3:
                        return 'C';
                    case 4:
                        return 'D';
                }
            }
            return null;
        }

        private static int groupRandomGenerator(int i)
        {
            var randomObject = new Random(Guid.NewGuid().GetHashCode());
            return randomObject.Next(i)+1;
        }
        private static int courseIdRandomGenerator(List<Course> courseList)
        {
           
            var randomObject = new Random(Guid.NewGuid().GetHashCode());
            return randomObject.Next(courseList.Count);
        }
        private static string PESELRandomGenerator(string [] studentsPesels)
        {
            var randomObject = new Random(Guid.NewGuid().GetHashCode());
            return studentsPesels[randomObject.Next(studentsPesels.Count())];
        }
        private static int activeRandomGenerator(char? mark)
        {
            if (mark == null) // if he's no mark already
            { 
                var randomObject = new Random(Guid.NewGuid().GetHashCode()); 
                return randomObject.Next(2);
            }
            return 1;
        }
      
        static public void saveToFile(List<Participation> participationList, char separator, string path)
        {
            string[] strArray = new string[participationList.Count];
            for (int i = 0; i < participationList.Count; i++)
            {
                strArray[i] = participationList.ElementAt(i).ID_participation.ToString()
                              + separator
                              + participationList.ElementAt(i).Mark
                              + separator
                              + participationList.ElementAt(i).Active
                              + separator
                              + participationList.ElementAt(i).PESEL_student
                              + separator
                              + participationList.ElementAt(i).ID_course
                              + separator
                              + participationList.ElementAt(i).ID_group;
            }
            writeToFile(strArray, path);
        }
        static public void writeToFile(string[] strArray, string path)
        {
            var encoding = System.Text.Encoding.GetEncoding("ISO-8859-2");
            using (StreamWriter outputFile = new StreamWriter(path, false))
            {
                foreach (var element in strArray)
                {
                    outputFile.WriteLine(element, encoding);
                }
            }
            Cursor.Current = Cursors.Default;
        }
    }
}
