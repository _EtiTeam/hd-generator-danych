﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generator
{
    class Student : Person
    {
        public List<int> groupList { get; set; }
       
        public Student()
            : base()
        {
            groupList = new List<int>();
        }

        static public void saveToExcel(
            List<Participation> participationList,  char separator, string path, bool append)
        {
            var nextSpearator =  separator ;
            string[] strArray = new string[participationList.Count];

            Parallel.For(0, participationList.Count, i =>
            {
                var people = participationList[i].Student;

                strArray[i] =  people.Pesel + nextSpearator
                   + people.Name + nextSpearator
                   + people.Surname + nextSpearator
                   + people.Address + nextSpearator
                   + people.CityCode + nextSpearator
                   + people.City + nextSpearator
                   + people.PhoneNumber.ToString() + nextSpearator
                   + participationList[i].ID_group ;
            });

            if (append)
                UniversalFunctions.appendToFile(strArray, path + ".csv");
            else
                UniversalFunctions.writeToFile(strArray, path + ".csv");
        }
    }
}
