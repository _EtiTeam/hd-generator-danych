﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generator
{
    // Dictionary which increse preformance for indexed search
    class ExtendedDictionary<TKey,TValue>
        : IDictionary<TKey,TValue>
    {
        IDictionary<TKey,TValue> _dict;
        List<TKey> _keys;

        public ExtendedDictionary(IDictionary<TKey,TValue> dict)
        {
            _dict = dict;
            _keys = _dict.Keys.AsParallel().ToList();
        }

        public TValue ElementAt(int index)
        {
            return _dict[_keys[index]];
        }

        public TKey KeyAt(int index)
        {
            return _keys[index];
        }

        #region implementIDictionary

        public void Add(TKey key, TValue value)
        {
            _dict.Add(key, value);
            _keys.Add(key);
        }

        public bool ContainsKey(TKey key)
        {
            return _dict.ContainsKey(key);
        }

        public ICollection<TKey> Keys
        {
            get { return _dict.Keys; }
        }

        public bool Remove(TKey key)
        {
            if (_dict.Remove(key))
            {
                _keys.Remove(key);
                return true;
            }
            return false;
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            return _dict.TryGetValue(key, out value);
        }

        public ICollection<TValue> Values
        {
            get { return _dict.Values; }
        }

        public TValue this[TKey key]
        {
            get
            {
                return _dict[key];
            }
            set
            {
                _dict[key] = value;
            }
        }

        public void Add(KeyValuePair<TKey, TValue> item)
        {
            _dict.Add(item);
            _keys.Add(item.Key);
        }

        public void Clear()
        {
            _dict.Clear();
            _keys.Clear();
        }

        public bool Contains(KeyValuePair<TKey, TValue> item)
        {
            return _dict.Contains(item);
        }

        public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            _dict.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return _dict.Count; }
        }

        public bool IsReadOnly
        {
            get { return _dict.IsReadOnly; }
        }

        public bool Remove(KeyValuePair<TKey, TValue> item)
        {
            return Remove(item.Key);
        }

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return _dict.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _dict.GetEnumerator();
        }

        #endregion
    }
}
