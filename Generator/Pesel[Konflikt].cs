﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generator
{
    class Pesel
    {
        static private short minYear = 1900;
        static private short maxYear = 2004;

        public Pesel()
        {   
        }

        public struct PeselData
        {
            public string pesel;
            public DateTime birthDate;
        }

        public PeselData Generate()
        {
            return Generate(new Random(Guid.NewGuid().GetHashCode()));
        }

        // outside Random is needed to increase performance of the function
        public PeselData Generate(Random rnd)
        {
            var peselStringBuilder = new StringBuilder();
            DateTime birthDate = Date.GenerateDate(minYear, maxYear);

            // generating the pesel
            AppendPeselDate(birthDate, peselStringBuilder);
            peselStringBuilder.Append(GenerateRandomNumbers(rnd, 4));
            peselStringBuilder.Append(PeselCheckSum.Calculate(peselStringBuilder.ToString()));
            
            PeselData peselData = new PeselData();
            peselData.pesel = peselStringBuilder.ToString();
            peselData.birthDate = birthDate;
            return peselData;
        }       

        private string GetPeselMonthShiftedByYear(DateTime date)
        {
            if(date.Year < minYear || date.Year > maxYear)
            {
                throw new NotSupportedException(string.Format("PESEL for year: {0} is not supported", date.Year));
            }

            int monthShift = (int)((date.Year - 1900) / 100) * 20;

            return (date.Month + monthShift).ToString("00");
        }

        private void AppendPeselDate(DateTime date, StringBuilder builder)
        {
            builder.Append((date.Year % 100).ToString("00"));
            builder.Append(GetPeselMonthShiftedByYear(date));
            builder.Append(date.Day.ToString("00"));
        }

        private string GenerateRandomNumbers(Random _random, int numbersCount)
        {
            int maxValue = (int)Math.Pow(10, numbersCount);
            string format = "D" + numbersCount;

            return _random.Next(maxValue).ToString(format);
        }
    }
}
