﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Generator
{
    class Person
    {
        #region variables
        public String Pesel     { get; set; }
        public DateTime Date    { get; set; }
        public int Age          { get; set; }
        public String Name      { get; set; }
        public String Surname   { get; set; }
        public String Address   { get; set; }
        public String City      { get; set; }
        public String CityCode  { get; set; }
        public int PhoneNumber  { get; set; }


        // static variables
        // row stucture: city-code, street, city 
        private static String[][] addresses;
        public static String[] NameList { get; private set; }
        public static String[] SurnameList { get; private set; }

        // variables used in Lector
        protected static object lockObject = new object();
        protected static bool isFirstGenerating = true; 

        #endregion
        #region logic

        public Person()
        {
        }

        // constructor for loading data
        static Person()
        {
            var encoding = System.Text.Encoding.GetEncoding("ISO-8859-2");

            // loading name and surnames from file

            try
            {
                NameList = File.ReadAllLines("imiona.txt", encoding);
                SurnameList = File.ReadAllLines("nazwiska.txt", encoding);

                // reading post codes
                var lines = File.ReadAllLines("kody_pocztowe.csv", encoding).Select(a => a.Split(';'));
                addresses = (from line in lines
                    select (
                        (from piece in line select piece).ToArray())
                    ).ToArray();
            }
            catch (FileNotFoundException e)
            {
                MessageBox.Show("Text source file not found!\n"+ e.FileName,"File not found error!");
                Application.Exit();
            }
        }

        private void generateName(Random rnd)
        {
            Name = NameList[rnd.Next(NameList.Count())];
        }

        private void genereteSurname(Random rnd)
        {
            Surname = SurnameList[rnd.Next(SurnameList.Count())];
        }

        private void generateAddress(Random rnd)
        {
            int idx = rnd.Next(addresses.Count());
            var tuple = addresses[idx];
            var house_number = (rnd.Next(200) + 1); // from 1 to 200
            CityCode = tuple[0];
            Address = tuple[1] + " " + house_number;
            City = tuple[2];
        }

        private void recalculateAge(DateTime now)
        {
            Age = Generator.Date.YearDifference(Date, now);
        }

        public virtual void Generate(DateTime now, Random rnd)
        {
            generateAddress(rnd);

            // generate name and surname
            generateName(rnd);
            genereteSurname(rnd);
            
            // generate pesel and birthDate
            var pesel = new Pesel();
            var peselData = pesel.Generate(rnd);

            if (peselData.isCorrect() == false)
                throw new Exception("error with pesel data");
            
            Pesel = peselData.pesel;
            Date = peselData.birthDate;
            recalculateAge(now);

            // generate phone number

            PhoneNumber = rnd.Next(5,10); // first digit [5-9]
            for (var i = 0; i < 8; i++){
                PhoneNumber *= 10;              // shift to the left
                PhoneNumber += rnd.Next(0, 10); // other digits [0-9]
            }
        }

        public virtual void Generate2(DateTime now, Random rnd)
        {
            // there is chance for changing address
            const double chance = 0.2; // percentage
            if (rnd.NextDouble() < chance)
                generateAddress(rnd);

            if (rnd.NextDouble() < chance)
                genereteSurname(rnd);

            recalculateAge(now);
        }

        static public void GenerateUniquePeople<T>(ConcurrentDictionary<string, T> people, ConcurrentDictionary<string,byte> generatedPeople, DateTime now, int numberOfPeople)
            where T : Person, new()
        {
            isFirstGenerating = true;
            Cursor.Current = Cursors.WaitCursor;

            //Person outReduntantVar = new Person();
            Parallel.For(0, numberOfPeople,
                     index =>
                     {
                         var rnd = new Random(Guid.NewGuid().GetHashCode());

                         //paralell generating PESEL and birth date
                         T person = new T(); // one thread = one person
                         byte nothing = 0;
                         while (true)
                         {
                             person.Generate(now, rnd);
                             lock (generatedPeople)
                             {
                                 if (generatedPeople.ContainsKey(person.Pesel) == false)
                                 //check if a person is already generated anywhere
                                 {  //if it's not generated already, add to generated goup of people e.g. students
                                     generatedPeople.GetOrAdd(person.Pesel, nothing);
                                     people.GetOrAdd(person.Pesel, person);
                                     break;
                                 }
                             }
                         }
                     });
        }


        static public void GenerateUniquePeople2<T>(ConcurrentDictionary<string, T> people2, ConcurrentDictionary<string, T> people1, ConcurrentDictionary<string, byte> generatedPeople, DateTime now, int numberOfNewPeople)
            where T : Person, new()
        {
            Cursor.Current = Cursors.WaitCursor;
            Person outReduntantVar = new Person();

            // generate newPeople
            Parallel.For(0, numberOfNewPeople,
                     index =>
                     {
                         var rnd = new Random(Guid.NewGuid().GetHashCode());

                         //paralell generating PESEL and birth date
                         T person = new T(); // one thread = one person
                         byte nothing = 0;
                         while (true)
                         {
                             person.Generate(now, rnd);

                             lock (generatedPeople)
                             {
                                 if (!generatedPeople.TryGetValue(person.Pesel, out nothing))
                                 //check if a person is already generated anywhere
                                 {  //if it's not generated already, add to generated group of people e.g. students
                                     generatedPeople.GetOrAdd(person.Pesel, nothing);
                                     people2.GetOrAdd(person.Pesel, person);
                                     break;
                                 }
                             }
                         }
                     });

            // changing existing people
            Parallel.ForEach(people1, tuple => tuple.Value.Generate2(now, new Random(Guid.NewGuid().GetHashCode())));
        }


        #endregion
        #region saving

        public virtual String [] getDataArray()
        {
            return new [] { 
                Pesel, 
                Name,
                Surname,
                Date.ToShortDateString(), 
            };
        }
        public virtual String[] getExcelDataArray()
        {
            //overwritten in Student and Lector classes
            throw new Exception("method Person->getExcelDataArray must be overwritten!");
        }

        static public void saveToFile<T>( ConcurrentDictionary<string, T> peoplesDict,char separator, string path, bool append)
            where T: Person
        {
            string[] strArray = new string[peoplesDict.Count];
            var peoples = peoplesDict.Values.ToList<T>(); // to use it as paraller

            Parallel.For(0, peoples.Count, i =>
            {
                var dataArray = peoples[i].getDataArray();
                strArray[i] = ""; // clear the string
                foreach (var g in dataArray)
                    strArray[i] += g + separator;
                strArray[i] = strArray[i].Remove(strArray[i].Count() - 1); // remove last separator
            });

            if (append)
                UniversalFunctions.appendToFile(strArray, path);
            else
                UniversalFunctions.writeToFile(strArray, path);
        }

        static public void saveToExcel<T>(ConcurrentDictionary<string, T> peoplesDict, char separator, string path, bool append)
            where T : Person
        {
            string[] strArray = new string[peoplesDict.Count];
            var peoples = peoplesDict.Values.ToList<T>(); // to use it as paraller

            Parallel.For(0, peoples.Count, i =>
            {
                var dataArray = peoples[i].getExcelDataArray();
                strArray[i] = ""; // clear the string
                foreach (var g in dataArray)
                    strArray[i] +=  g  + separator;
                strArray[i] = strArray[i].Remove(strArray[i].Count() - 1); // remove last separator
            });

            if (append)
                UniversalFunctions.appendToFile(strArray, path+".csv");
            else
                UniversalFunctions.writeToFile(strArray, path+".csv");
        }

    }

        #endregion
}
