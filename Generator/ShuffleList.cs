﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generator
{
    class ShuffleList<T>
        :IList<T>
    {
        private IList<T> _list;
        private int _index;
        private bool _canMix;

        public ShuffleList(IList<T> list, bool canMixSeveralTimes = true)
        {
            _list = list;
            _index = _list.Count; // out of range, first usage shuffles list
            _canMix = canMixSeveralTimes;

            if (_canMix == false) // only one first shuffle
                shuffle();
        }

        public void Shuffle()
        {
            if (_canMix == false)
                throw new Exception("Bad usage. To many Next(). Parameter canMixSeveralTimes is setted on false");

            shuffle();
        }

        private void shuffle()
        {
            UniversalFunctions.Shuffle(_list, new Random(Guid.NewGuid().GetHashCode()));
            _index = 0;
        }

        public int Remain
        {
            get
            {
                return _list.Count - _index;
            }
        }

        public T Next()
        {
            if (Remain <= 0) 
                Shuffle();
            return _list[_index++];
        }

        #region interfaceImplement
        public T this[int index]
        {
            get
            {
                return _list[index];
            }
            set
            {
                _list[index] = value;
            }
        }

        public int IndexOf(T item)
        {
            return _list.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            _list.Insert(index, item);
        }

        public void RemoveAt(int index)
        {
            _list.RemoveAt(index);
        }

        public void Add(T item)
        {
            _list.Add(item);
        }

        public void Clear()
        {
            _list.Clear();
        }

        public bool Contains(T item)
        {
            return _list.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            _list.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return _list.Count; }
        }

        public bool IsReadOnly
        {
            get { return _list.IsReadOnly; }
        }

        public bool Remove(T item)
        {
            return _list.Remove(item);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _list.GetEnumerator();
        }
    }
        #endregion
}
