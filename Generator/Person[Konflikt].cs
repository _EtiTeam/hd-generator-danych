﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Generator
{
    class Person
    {
        #region variables
        public String Pesel { get; set; }
        public DateTime Date { get; set; }
        public String Name { get; set; }
        public String Surname { get; set; }
        public String Address { get; set; }
        public String City { get; set; }
        public String CityCode { get; set; }
        public int PhoneNumber { get; set; }

        protected Random rnd;

        // static variables
        // row stucture: city-code, street, city 
        private static String[][] addresses;
        public static string[] NameList { get; private set; }
        public static string[] SurnameList { get; private set; }


        #endregion
        #region logic

        public Person()
        {
            rnd = new Random(Guid.NewGuid().GetHashCode());
        }

        // constructor for loading data
        static Person()
        {
            var encoding = System.Text.Encoding.GetEncoding("ISO-8859-2");

            // reading name and surnames from file
            NameList = File.ReadAllLines("imiona.txt", encoding);
            SurnameList = File.ReadAllLines("nazwiska.txt", encoding);

            // reading post codes
            var lines = File.ReadAllLines("kody_pocztowe.csv", encoding).Select(a => a.Split(';'));
            addresses = (from line in lines
                       select (
                           (from piece in line select piece).ToArray())
                           ).ToArray();
        }

        private void generateAddress()
        {
            int idx = rnd.Next(addresses.Count());
            var tuple = addresses[idx];
            var house_number = (rnd.Next(200) + 1); // from 1 to 200
            CityCode = tuple[0];
            Address = tuple[1] + " " + house_number;
            City = tuple[2];
        }

        public virtual void Generate()
        {
            generateAddress();

            // generate name and surname
            Name = NameList[rnd.Next(NameList.Count())];
            Surname = SurnameList[rnd.Next(SurnameList.Count())];
            
            // generate pesel and birthDate
            var pesel = new Pesel();
            var peselData = pesel.Generate(rnd);
            Pesel = peselData.pesel;
            Date = peselData.birthDate;

            // generate phone number
            PhoneNumber = rnd.Next(5,10); // first digit [5-9]
            for (var i = 0; i < 9; ++i){
                PhoneNumber *= 10;              // shift to the left
                PhoneNumber += rnd.Next(0, 10); // other digits [0-9]
            }
        }

        public virtual void Generate2()
        {
            // there is chance for changing address
            const double chance = 0.2; // percentage
            if (rnd.NextDouble() < chance)
                generateAddress();
        }

        #endregion
        #region saving

        public virtual String [] getDataArray()
        {
            return new [] { 
                Pesel, 
                Date.ToShortDateString(), 
                Name,
                Surname
            };
        }

        static public void saveToFile<T>( ConcurrentDictionary<string, T> peoples,char separator,string path)
            where T: Person
        {
            string [] strArray = new string[peoples.Count];
            var i = 0;

            foreach(var people in peoples)
            {
                var dataArray = people.Value.getDataArray();
                strArray[i] = ""; // clear the string
                foreach (var g in dataArray)
                    strArray[i] += g + separator;
                strArray[i] = strArray[i].Remove(strArray[i].Count() - 1); // remove last separator
                ++i;
            }
            writeToFile(strArray, path);
        }

        static public void writeToFile(string [] strArray,string path)
        {
            var encoding = System.Text.Encoding.GetEncoding("ISO-8859-2");
            using (StreamWriter outputFile = new StreamWriter(path, false))
            {
                foreach (var element in strArray)
                {
                    outputFile.WriteLine(element, encoding);
                }
            }
            Cursor.Current = Cursors.Default;
        }
    }

        #endregion
}
