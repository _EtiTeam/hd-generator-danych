﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace Generator
{
    static class UniversalFunctions
    {
        static public void Shuffle<T>(IList<T> collection, Random rnd){
            var currIdx = collection.Count;
            T tmpVal;
            int rndIdx;

            while (0 != currIdx){
                rndIdx = rnd.Next(currIdx);
                --currIdx;

                tmpVal = collection[currIdx];
                collection[currIdx] = collection[rndIdx];
                collection[rndIdx] = tmpVal;
            }

        }

        #region saving

        static public void writeToFile(string[] strArray, string path)
        {
            var encoding = System.Text.Encoding.GetEncoding("ISO-8859-2");
            try
            {
                using (StreamWriter outputFile = new StreamWriter(path, false))
                {
                    foreach (var element in strArray)
                    {
                        outputFile.WriteLine(element, encoding);
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid path!:\n" + path + "\nTry to generate and save again", "Invalid path error!");
                
            }
        }
        static public void appendToFile(string[] strArray, string path)
        {
            if (File.Exists(path) == false)
                throw new IOException("file not exists");

            var encoding = System.Text.Encoding.GetEncoding("ISO-8859-2");
            using (StreamWriter outputFile = new StreamWriter(path, true))
            {
                foreach (var element in strArray)
                {
                    outputFile.WriteLine(element, encoding);
                }
            }
        }
        #endregion

        //public static IEnumerable<TValue> RandomValues<TKey, TValue>(this IDictionary<TKey, TValue> dict)
        //{
        //    Random rand = new Random();
        //    List<TValue> values = Enumerable.ToList(dict.Values);
        //    int size = dict.Count;
        //    while (true)
        //    {
        //        yield return values[rand.Next(size)];
        //    }
        //}

        public static TKey GetFirstKey<TKey, TValue>(this IDictionary<TKey, TValue> dict)
        {
            foreach (var k in dict.Keys)
            {
                return k;
            }

            throw new Exception("error");
        }
    }
}
