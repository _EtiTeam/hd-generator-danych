﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Windows.Forms;

namespace Generator
{
    class Lesson
    {
        public int ID { get; private set; }
        public string Subject { get; private set; }
        public DateTime Date { get; private set; }
        public string FK_lector_pesel { get; private set; }
        public int FK_ID_kurs { get; private set; }

        private Random rnd;

        static private DateTime lastDate; // used in 2nd generation

        private static int ID_counter = 0;
        private static string[] subjectArray;

        static Lesson()
        {
            var encoding = System.Text.Encoding.GetEncoding("ISO-8859-2");         

            // reading name and surnames from file
            subjectArray = File.ReadAllLines("titles.txt", encoding);
        }

        public Lesson()
        {
            rnd = new Random(Guid.NewGuid().GetHashCode());
        }

        public void Generate(string[] lectorsPesels, List<Course> courses, DateTime now)
        {          
            ID = ID_counter++;
            Subject = subjectArray[rnd.Next(subjectArray.Count())];
            FK_lector_pesel = lectorsPesels[rnd.Next(lectorsPesels.Count())];

            // DATE GENERATING
            Course associatedCourse;
            do
            {
                associatedCourse = courses.ElementAt(rnd.Next(courses.Count()));
                FK_ID_kurs = associatedCourse.ID_course;

                lastDate = (now < associatedCourse.EndDate) ? now : associatedCourse.EndDate;
            } while (lastDate < associatedCourse.StartDate);    // select the course while the data range isn't ok

            Date = Generator.Date.GenerateDate(associatedCourse.StartDate, lastDate);
        }

        // it bases on the LastDate in course
        public void Generate2(ConcurrentDictionary<string, Lector> lectors, List<Course> courses, DateTime now)
        {
            if (lastDate == null)
                throw new Exception("Bad usage. First use Generate function");

            Course associatedCourse;

            ID = ID_counter++;
            Subject = subjectArray[rnd.Next(subjectArray.Count())];
            FK_lector_pesel = lectors.ElementAt(rnd.Next(lectors.Count)).Key;
            

            // DATE GENERATING
            // first date must be StartDate of the course or lastDate from previous generating function
            DateTime firstDate;
            do {
                associatedCourse = courses.ElementAt(rnd.Next(courses.Count()));
                FK_ID_kurs = associatedCourse.ID_course;

                firstDate = (lastDate > associatedCourse.StartDate) ? lastDate : associatedCourse.StartDate;
            } while (firstDate > associatedCourse.EndDate);     // select the course while the data range isn't ok
            Date = Generator.Date.GenerateDate(firstDate, now);  
        }

        public static void Generate(List<Lesson> lessonList, ConcurrentDictionary<string, Lector> lectors, List<Course> courses, DateTime now, int count)
        {
            string[] lectorsPesels = lectors.Keys.ToArray();
            for (int i = 0; i < count; i++)
            {
                Lesson newLesson = new Lesson();
                newLesson.Generate(lectorsPesels, courses, now);
                lessonList.Add(newLesson);
            }
        }

        public static void Generate2(List<Lesson> lessonList, ConcurrentDictionary<string, Lector> lectors, List<Course> courses, DateTime now, int count)
        {
            for (int i = 0; i < count; i++)
            {
                Lesson newLesson = new Lesson();
                newLesson.Generate2(lectors, courses, now);
                lessonList.Add(newLesson);
            }
        }


        private String[] getDataArray()
        {
            return new[] { 
                ID.ToString(), 
                Subject, 
                Date.ToShortDateString(),
                FK_lector_pesel.ToString(),
                FK_ID_kurs.ToString()
            };
        }

        public static void SaveToFile(List<Lesson> LessonList, char separator, string pathCourses)
        {
            string[] strArray = new string[LessonList.Count];
            for (int i = 0; i < LessonList.Count; i++)
            {
                var dataArray = LessonList.ElementAt(i).getDataArray();
                strArray[i] = ""; // clear the string
                foreach (var g in dataArray)
                    strArray[i] += g + separator;
                strArray[i] = strArray[i].Remove(strArray[i].Count() - 1); // remove last separator
            }
            writeToFile(strArray, pathCourses);
        }

        static public void writeToFile(string[] strArray, string path)
        {
            var encoding = System.Text.Encoding.GetEncoding("ISO-8859-2");
            using (StreamWriter outputFile = new StreamWriter(path, false))
            {
                foreach (var element in strArray)
                {
                    outputFile.WriteLine(element, encoding);
                }
            }
            Cursor.Current = Cursors.Default;
        }
    }
}
