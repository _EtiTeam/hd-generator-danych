﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Generator
{
    class Course
    {
        public Exam Exam { get; private set; }
        public int ID_course { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int ID_exam { get; set; }

        private static int ID_counter = 0;

        public static void Generate(List<Course> courseList, List<Exam> examList, DateTime now, int count)
        {
            var randomObject = new Random(Guid.NewGuid().GetHashCode());
            
            Exam associatedExam;
            for (int i = 0; i < count; i++)
            {
                Course NewCourse = new Course();
                NewCourse.ID_course = Interlocked.Increment(ref ID_counter);

                associatedExam = examList[randomObject.Next(examList.Count)];
                NewCourse.Exam = associatedExam;
                NewCourse.ID_exam = associatedExam.ID_exam;
                DateTime ExamDate = associatedExam.Date;

                //StartDate - variable based on specified time range before ExamDate
                NewCourse.StartDate = Generator.Date.GenerateDate(ExamDate.AddYears(-1), ExamDate.AddMonths(-3), randomObject);
                if (NewCourse.StartDate > now)
                    NewCourse.StartDate = Generator.Date.GenerateDate(now.AddYears(-1), now.AddMonths(-3), randomObject);

                NewCourse.EndDate = Generator.Date.GenerateDate(ExamDate.AddDays(-10), ExamDate, randomObject);
                
                courseList.Add(NewCourse);
            }
        }

        // actual method not used - new courses are creating only for new Exam
        public static void Generate2(List<Course> courseList2, List<Exam> examList1, List<Exam> examList2, int count)
        {
            throw new Exception("Function actual not used");

            // merging examLists
            var examList = examList1.Concat(examList2).ToList();

            // other instruction are the same for all generations
            Generate(courseList2, examList1, new DateTime(1990,1,1), count);
        }

        static public void saveToFile(List<Course> CourseList, char separator, string path, bool append)
        {
            string[] strArray = new string[CourseList.Count];
            Parallel.For(0, CourseList.Count, i =>
            {              
                strArray[i] = CourseList[i].ID_course.ToString()
                              + separator
                              + CourseList[i].StartDate.ToShortDateString()
                              + separator
                              + CourseList[i].EndDate.ToShortDateString()
                              + separator
                              + CourseList[i].ID_exam.ToString();
            });

            if (append)
                UniversalFunctions.appendToFile(strArray, path);
            else
                UniversalFunctions.writeToFile(strArray, path);
        }

    }
}
