﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace Generator
{
    public partial class Form1 : Form
    {
        #region variables
        private Pesel TestPesel;
        private List<Exam> ExamList;
        private List<Lesson> LessonList;
        private List<Course> CourseList;
        private List<Participation> ParticipationList;
        private SaveFileDialog saveFileDialog;

        private string pathStudents;
        private string pathLectors;
        private string pathExams;
        private string pathCourses;
        private string pathLessons;
        private string pathParticipation;
        private int count;

        private ConcurrentDictionary<string, Lector> lectors;
        private ConcurrentDictionary<string, Lector> newLectors;
        private ConcurrentDictionary<string, Person> students;
        private ConcurrentDictionary<string, Person> newStudents;
        private ConcurrentDictionary<string, byte> generatedPeople; // value is't needed
        private DateTime date;

        #endregion
        #region initialization
        public Form1()
        {
            InitializeComponent();
            lectors = new ConcurrentDictionary<string, Lector>();
            students = new ConcurrentDictionary<string, Person>();
            generatedPeople = new ConcurrentDictionary<string, byte>();
            
            ExamList = new List<Exam>();
            LessonList = new List<Lesson>();
            CourseList = new List<Course>();
            ParticipationList = new List<Participation>();
            saveFileDialog = new SaveFileDialog();

            //time-changing filename pattern used to allow multiple file version testing
            var metaDataFileName = DateTime.Now.ToLongTimeString().Replace(':', '.') + ".txt";
            pathStudents = "generatedStudents_" + metaDataFileName;
            pathLectors = "generatedLectors_" + metaDataFileName;
            pathExams = "generatedExams_" + metaDataFileName;
            pathCourses = "generatedCourses_" + metaDataFileName;
            pathLessons = "generatedLessons_" + metaDataFileName;
            pathParticipation = "generatedParticipations_" + metaDataFileName;
            count = 15;         // TODO po co to???

            trackBar1.Maximum = 1000000;
            trackBar1.Value = 25000;
            countBox.Text = "25000";
        }
        private void Form1_Load(object sender, EventArgs e)
        {
           // var encoding = System.Text.Encoding.GetEncoding("ISO-8859-2");
            //test = new Pesel();
            //peselBox.Text = TestPesel.Generate().Pesel;

            //NameList = new List<string>(logFile);
            //logFile = File.ReadAllLines("nazwiska.txt", encoding);
            //SurnameList = new List<string>(logFile);
            Person person = new Person();
            person.Generate();
            peselBox.Text = person.Pesel;
            nameBox.Text = person.Name;
            
            progressBar1.Value = 0;
            //generateButton.Enabled = false;
            date = dateTimePicker1.Value;
        }
        private void peselBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void generateButton_Click(object sender, EventArgs e)
        {
            generateUniquePeople(students, trackBar1.Value);
            Person.saveToFile(students, '|', pathStudents);
            progressBar1.Value = 20;

            generateUniquePeople(lectors, trackBar1.Value);
            Person.saveToFile(lectors, '|', pathLectors);
            progressBar1.Value = 40;

            Exam.Generate(ExamList, new DateTime(2012, 1, 1), date, trackBar1.Value);
            Exam.saveToFile(ExamList, '|', pathExams);
            progressBar1.Value = 50;

            Course.Generate(CourseList, ExamList, trackBar1.Value);
            Course.saveToFile(CourseList, '|', pathCourses);
            progressBar1.Value = 60;

            Lesson.Generate(LessonList, lectors, CourseList, date, trackBar1.Value);
            Lesson.SaveToFile(LessonList, '|', pathLessons);
            progressBar1.Value = 70;

            Participation.Generate(ParticipationList, CourseList, students, trackBar1.Value, dateTimePicker1.Value);
            Participation.saveToFile(ParticipationList, '|', pathParticipation);
            progressBar1.Value = 100;
        
            //generateButton2.
        }

        private void generateButton2_Click(object sender, EventArgs e)
        {
            generateUnique2People(newStudents, students, (int)countBox2.Value);
           // TODO zapis z dwoch kolejkcji do jednego pliku
            // Person.saveToFile(students, '|', pathStudents);
            progressBar1.Value = 20;

            generateUnique2People(newLectors, lectors, (int)countBox2.Value);
            //Person.saveToFile(students, '|', pathStudents);
            progressBar1.Value = 20;
        }

        #endregion
        #region logic

        private void generateUniquePeople<T>(ConcurrentDictionary<string, T> people, int numberOfPeople)
            //where T: Person //e.g. students, lectors
            where T: Person, new()
        {         
            Cursor.Current = Cursors.WaitCursor;
            
            //Person outReduntantVar = new Person();
            Parallel.For(0, numberOfPeople,
                     index =>
                     {
                         //paralell generating PESEL and birth date
                         T person = new T(); // one thread = one person
                         byte nothing = 0;
                         while (true)
                         {
                             person.Generate();
                             lock (generatedPeople)
                             {
                                 if (!generatedPeople.TryGetValue(person.Pesel, out nothing))
                                 //check if a person is already generated anywhere
                                 {  //if it's not generated already, add to generated group of people e.g. students
                                     generatedPeople.GetOrAdd(person.Pesel, nothing);
                                     people.GetOrAdd(person.Pesel, person);
                                     break;
                                 }
                             }
                         }
                     });
            //assign generated values to GUI's TextBoxes (test only)
            peselBox.Text = people.ElementAt(1).Key;
           

            //randomize name and surname to generated values
           //// int random = new Random().Next() % NameList.Count;
           // nameBox.Text = NameList[random];
           // random = new Random().Next() % NameList.Count;
           // surnameBox.Text = SurnameList[random];
            progressBar1.Value = 100;
            
           
        }


        private void generateUnique2People<T>(ConcurrentDictionary<string, T> newPeople, ConcurrentDictionary<string, T> oldPeople, int numberOfNewPeople)
            where T : Person, new()
        {
            Cursor.Current = Cursors.WaitCursor;
            Person outReduntantVar = new Person();

            // generate newPeople
            Parallel.For(0, numberOfNewPeople,
                     index =>
                     {
                         //paralell generating PESEL and birth date
                         T person = new T(); // one thread = one person
                         byte nothing = 0;
                         while (true)
                         {
                             person.Generate();

                             lock (generatedPeople)
                             {
                                 if (!generatedPeople.TryGetValue(person.Pesel, out nothing))
                                 //check if a person is already generated anywhere
                                 {  //if it's not generated already, add to generated group of people e.g. students
                                     generatedPeople.GetOrAdd(person.Pesel, nothing);
                                     newPeople.GetOrAdd(person.Pesel, person);
                                     break;
                                 }
                             }
                         }
                     });

            // changing existing people
            Parallel.ForEach(oldPeople, tuple => tuple.Value.Generate2());

            //assign generated values to GUI's TextBoxes (test only)
            peselBox.Text = newPeople.ElementAt(1).Key;
        }


        #endregion
        #region saving

        static public void writeToFile(string[] strArray, string path)
        {
            var encoding = System.Text.Encoding.GetEncoding("ISO-8859-2");
            using (StreamWriter outputFile = new StreamWriter(path, false))
            {
                foreach (var element in strArray)
                {
                    outputFile.WriteLine(element, encoding);
                }
            }
            Cursor.Current = Cursors.Default;
        }
        static public void appendToFile(string[] strArray, string path)
        {
            if (File.Exists(path) == false)
                throw new IOException("file not exists");

            var encoding = System.Text.Encoding.GetEncoding("ISO-8859-2");
            using (StreamWriter outputFile = new StreamWriter(path,true))
            {
                foreach (var element in strArray)
                {
                    outputFile.WriteLine(element, encoding);
                }
            }
        }
        #endregion
        #region other

        private void label1_Click(object sender, EventArgs e)
        {
        }
        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void localizeButton_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                pathStudents = saveFileDialog.FileName;
                generateButton.Enabled = true;
                generateButton.BackColor = Color.GreenYellow;
            }
            
        }

        private void countBox_TextChanged(object sender, EventArgs e)
        {
            int a;
            bool success = Int32.TryParse(countBox.Text, out a);
            if (success == true && a < trackBar1.Maximum)
            {
                trackBar1.Value = a;
                count = a;
            }
        }
        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            trackBar1.Value = count;
            countBox.Text = trackBar1.Value.ToString();
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
        #endregion