﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Generator
{
    class Participation
    {
        public int ID_participation { get; set; }
        public char? Mark {get; private set; }
        public int Active { get; private set; } //check if student participates in course
        public DateTime? ResignDate { get; private set; }
        public Student Student { get; private set; }
        public Course Course { get; private set; }
        public int ID_group { get; private set; }

        private static int groupCounter = 1; // with startnumbers
        private static int ID_counter = 0;

        #region HashCode_and_Comparing

        public override int GetHashCode()
        {
            return ID_participation.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var x = obj as Participation;
            if (x == null) throw new Exception("not implement");
            return ID_participation == x.ID_participation;
        }
        #endregion
        #region ParticipationGenerator
        public char? getMark()
        {
            return Mark;
        }
        public void setMark(char? mark)
        {   //mark input validation
            if (mark == 'A' || mark == 'B' || mark == 'C' || mark == 'D' || mark == null)
                Mark = mark;
        }
        private void markRandomGenerator(Random rnd, DateTime now)
        {
            if (Active == 0)
            {
                Mark = null;
                return;
            }

            //var ID_exam = courseList.Find(x => x.ID_course == ID_course).ID_exam;
            // var exam = examList.Find(x => x.ID_exam == ID_exam);

            if (Course.Exam.Date < now)
            {
                switch (rnd.Next(4) + 1)
                {
                    case 1: Mark = 'A'; break;
                    case 2: Mark = 'B'; break;
                    case 3: Mark = 'C'; break;
                    case 4: Mark = 'D'; break;
                }
            }
            else
            {
                double chance = 0.2;
                if (rnd.NextDouble() < chance)
                    Active = 0;

                Mark = null;
            }
        }
        private static Student StudentRandomGenerator(List<Student> students, Random rnd)
        {
            return students[rnd.Next(students.Count())];
        }
        private void activeRandomGenerator(Random rnd, char? mark, DateTime now, DateTime? lastGenerationTime)
        {
            if (mark == null) // if he's no mark already
            {
                double chance = 0.2;
                var randomObject = new Random(Guid.NewGuid().GetHashCode()); 
                if (randomObject.NextDouble() <= chance){ // if 20% chance
                    Active = 0;
                    // select date range
                    DateTime start = lastGenerationTime.HasValue ? lastGenerationTime.Value : Course.StartDate;
                    DateTime end = now > Course.EndDate ? Course.EndDate : now;
                    ResignDate = Date.GenerateDate(start, end, randomObject);
                    return;
                }
            }
            Active = 1;
        }

        private static Participation GenerateOneParticipation(List<Course> courseList, Student student, DateTime now, DateTime? lastGenerationTime, int groups)
        {
            var rnd = new Random(Guid.NewGuid().GetHashCode());

            Participation NewParticipation = new Participation();
            NewParticipation.Student = student;
            NewParticipation.Course = courseList[rnd.Next(courseList.Count)];
            NewParticipation.ID_participation = Interlocked.Increment(ref ID_counter);
            // var examID = courseList[NewParticipation.ID_course].ID_exam;

            NewParticipation.activeRandomGenerator(rnd, NewParticipation.Mark, now, lastGenerationTime); // it set Active and ResignDate
            NewParticipation.markRandomGenerator(rnd, now);

            NewParticipation.ID_group = rnd.Next(groups) + 1;
            student.groupList.Add(NewParticipation.ID_group);

            return NewParticipation;
        }

        // generate participation for all sudent + countAddition participation for few students (he participant in more than 1 course)
        public static void Generate(List<Participation> participapationList, List<Course> courseList, List<Student> students, DateTime now, int groups, int countAddition)
        {
            var concurrentBag = new ConcurrentBag<Participation>();
            Parallel.ForEach(students, pesel =>
            {
                Participation NewParticipation = GenerateOneParticipation(courseList, pesel, now, null, groups);
                concurrentBag.Add(NewParticipation);
            });
            participapationList.AddRange(concurrentBag);


            var studentsIsOnDict = participapationList
                .AsParallel().GroupBy(x => x.Student.Pesel, y => y.Course.ID_course)
                .AsParallel().ToDictionary(x => x.Key, y => y.ToList());

            long error = 0;
            Random rnd = new Random(Guid.NewGuid().GetHashCode());
            UniversalFunctions.Shuffle(students, rnd);
            for (int i = 0, j = 0; i < countAddition; ++i, ++j) // j as index for students
            {
                if (error >= (long)courseList.Count * students.Count)
                    throw new Exception("wrong number of participations");
                if (j >= students.Count)
                    j = 0;

                var student = students[j];
                var studentIsOn = studentsIsOnDict[student.Pesel];

                if (studentIsOn.Count() >= courseList.Count)
                {
                    ++error;
                    --i;
                    continue;
                }

                int a = 0;
                Participation NewParticipation;
                do
                {
                    a++;
                    if (a>10000) throw new Exception("Infinite loop");
                    NewParticipation = GenerateOneParticipation(courseList, student, now, null, groups);
                } while (studentIsOn.Contains(NewParticipation.Course.ID_course));

                // saving 
                participapationList.Add(NewParticipation);

                if (studentsIsOnDict.ContainsKey(NewParticipation.Student.Pesel))
                    studentsIsOnDict[NewParticipation.Student.Pesel].Add(NewParticipation.Course.ID_course);
                else
                {
                    var newList = new List<int>();
                    newList.Add(NewParticipation.Course.ID_course);
                    studentsIsOnDict.Add(NewParticipation.Student.Pesel, newList);
                }
            }
        }

        // method:
        // -changing mark, 
        // -create new for new students
        public static void Generate2(List<Participation> participationList2, List<Participation> participationList, List<Course> courseList2, 
            List<Student> students1and2, List<Student> students2, DateTime now, DateTime lastGenerationTime, int groups, int countAddition)
        {
            Parallel.ForEach(participationList, x => x.markRandomGenerator(new Random(Guid.NewGuid().GetHashCode()), now));

            var concurrentBag = new ConcurrentBag<Participation>();
            Parallel.ForEach(students2, pesel =>
            {
                Participation NewParticipation = GenerateOneParticipation(courseList2, pesel, now, lastGenerationTime, groups);
                concurrentBag.Add(NewParticipation);
            });
            participationList2.AddRange(concurrentBag);

            var studentsIsOnDict = participationList.Concat(participationList2)
                .AsParallel().GroupBy(x => x.Student.Pesel, y => y.Course.ID_course)
                .AsParallel().ToDictionary(x => x.Key, y => y.ToList());

            long error = 0;
            Random rnd = new Random(Guid.NewGuid().GetHashCode());
            UniversalFunctions.Shuffle(students1and2, rnd);

            for (int i = 0, j = 0; i < countAddition; ++i, ++j)
            {
                if (error >= (long)courseList2.Count * students1and2.Count)
                    throw new Exception("wrong number of participations");
                if (j >= students1and2.Count)
                    j = 0;

                var student = students1and2[j];
                var studentIsOn = studentsIsOnDict[student.Pesel];

                int a = 0;
                Participation NewParticipation;
                do
                {
                    a++;
                    if (a > 10000) throw new Exception("Infinity loop");
                    NewParticipation = GenerateOneParticipation(courseList2, student, now, lastGenerationTime, groups);
                } while (studentIsOn.Contains(NewParticipation.Course.ID_course));

                //saving 
                participationList2.Add(NewParticipation);

                if (studentsIsOnDict.ContainsKey(NewParticipation.Student.Pesel))
                    studentsIsOnDict[NewParticipation.Student.Pesel].Add(NewParticipation.Course.ID_course);
                else
                {
                    var newList = new List<int>();
                    newList.Add(NewParticipation.Course.ID_course);
                    studentsIsOnDict.Add(NewParticipation.Student.Pesel, newList);
                }
            }

            
        }

#endregion

        public static void GenerateGroups(List<Participation> participationList, List<Presence> presenceList)
        {
            // GENERATING NEEDED DICTIONARIES
            var presenceExtendedList = presenceList
                .AsParallel().Join(participationList.AsParallel() // join to add Participation class
                    ,k => k.ID_participation
                    ,k => k.ID_participation
                    ,(l,r) => new KeyValuePair<Participation, Lesson>(r, l.Lesson)).ToList();
            var PtoL = presenceExtendedList
                .AsParallel().GroupBy(k => k.Key, e => e.Value)
                .AsParallel().ToDictionary(k => k.Key, e => e.ToList());
            var LtoP = presenceExtendedList
                .AsParallel().GroupBy(k => k.Value, e => e.Key)
                .AsParallel().ToDictionary(k => k.Key, e => e.ToList());
            ///////////////////////////////////
            // using a extended dictionary which have indexed access to Keys
            // at the cost of longer duration of Add and Remove functions
            // var PtoL = new ExtendedDictionary<Presence, Lesson>(PtoL_Slow);

            HashSet<Participation> pg; // participantGroup
            while (PtoL.Count > 0)
            //for (int g = 0; PtoL.Count > 0; ++g)
            {
                pg = new HashSet<Participation>();
                //pg.Add(PtoLKeyList.Next()); // first of the participation
                //pg.Add(PtoL.Keys.AsParallel().First());
                pg.Add(PtoL.GetFirstKey());
                
                // in next step there will be find relevant participations
                // which presences in the same lessons
                int i = 0; // flag which check if something changed
                int lastNoOfPg = 0;
                while (i != pg.Count)
                {
                    lastNoOfPg = i;
                    i = pg.Count;
                    addRelevantParticipants(lastNoOfPg, pg, PtoL, LtoP);
                }

                // set group number for all found participations
                Parallel.ForEach(pg, p => p.ID_group = groupCounter);
                Interlocked.Increment(ref groupCounter); // preparation for next group (next loop)
            }
        }

        private static void addRelevantParticipants(int lastNoOfPg,HashSet<Participation> pg, Dictionary<Participation, List<Lesson>> PtoL, Dictionary<Lesson, List<Participation>> LtoP)
        {
            ConcurrentDictionary<Lesson, byte> LS = new ConcurrentDictionary<Lesson,byte>();
             // LessonDictionary; second variable isn't needed
            
            var A = new List<List<Participation>>();
            for (int i = 0; i < pg.Count - lastNoOfPg; ++i) // create list for paraller execution
                A.Add(null);

            Parallel.For(lastNoOfPg, pg.Count, i => // TODO there could be check alway from 0
            {
                var index = i - lastNoOfPg;
                A[index] = new List<Participation>();
                var p = pg.ElementAt(i); // this participation

                //if (PtoL.ContainsKey(p)) // this participat could be already checked
                //{
                var LR = PtoL[p];        // relative Lessons
                foreach (var l in LR)
                {
                    if (LtoP.ContainsKey(l)) // this lesson could be already checked
                    {                        // so it is removed
                        A[index].AddRange(LtoP[l]);
                        LS.GetOrAdd(l, 0);
                    }
                }
                //}
                // reducing data
                //PtoL[p] = new List<Lesson>(); // clear, it is checked
                lock (PtoL) { PtoL.Remove(p); } // TODO it could be danger
            });

            // saving results
            foreach (var a in A)
                foreach (var x in a)
                    pg.Add(x); 

            // reduce number of lessons
            foreach (var l in LS)
                LtoP.Remove(l.Key);
        }


      
        static public void saveToFile(List<Participation> participationList, char separator, string path, bool append)
        {
            string[] strArray = new string[participationList.Count];
            for (int i = 0; i < participationList.Count; i++)
            {
                var ResignDate = participationList.ElementAt(i).ResignDate;
                strArray[i] = participationList.ElementAt(i).ID_participation.ToString()
                              + separator
                              + (ResignDate.HasValue ? ResignDate.Value.ToShortDateString() : "")
                              + separator
                              + participationList.ElementAt(i).ID_group
                              + separator
                              + participationList.ElementAt(i).Mark
                              + separator
                              + participationList.ElementAt(i).Course.ID_course
                              + separator
                              + participationList.ElementAt(i).Student.Pesel;
            }

            if (append)
                UniversalFunctions.appendToFile(strArray, path);
            else
                UniversalFunctions.writeToFile(strArray, path);
        }

    }
}
