﻿using System.Windows.Forms;

namespace Generator
{
    public class PercentageNumericUpDown : NumericUpDown
    {
        protected override void UpdateEditText()
        {
            this.Text = "+" + this.Value.ToString() + "%";
        }
    }
}