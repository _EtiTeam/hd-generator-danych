﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generator
{
    static class Date
    {
        private static Random randomObject;

        static Date()
        {
            randomObject = new Random(Guid.NewGuid().GetHashCode());
        }

        public static DateTime GenerateDate(int yearFrom, int yearTo)
        {
            return GenerateDate(new DateTime(yearFrom, 1, 1), new DateTime(yearTo, 1, 1));
        }

        public static DateTime GenerateDate(DateTime dateFrom, int yearTo)
        {
            return GenerateDate(dateFrom, new DateTime(yearTo, 1, 1));
        }

        public static DateTime GenerateDate(DateTime dateFrom, DateTime dateTo)
        {
            var days = (dateTo - dateFrom).Days;
            int randomDays;
            lock (randomObject)
            {
                randomDays = randomObject.Next(days);
            }
            return dateFrom.AddDays(randomDays); 
        }
    }
}
