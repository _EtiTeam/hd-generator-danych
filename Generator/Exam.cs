﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Generator
{
    class Exam
    {
        public int ID_exam{ get; set; }
        public DateTime Date { get; set; }
        public int Type{ get; set; } // 1 = FCE or 2 = CAE

        private static DateTime lastEndDate; // used in 2nd generation
        private static int ID_counter = 0;

        public static void Generate(List<Exam> examList, DateTime startDate, DateTime endDate, int count)
        {
            // TODO for better performance you should use array and paraller executing
            var randomObject = new Random(Guid.NewGuid().GetHashCode());
            for (int i = 0; i < count; i++)
            {
                Exam newExam = new Exam();
                newExam.ID_exam = Interlocked.Increment(ref ID_counter);
                newExam.Date = Generator.Date.GenerateDate(startDate, endDate, randomObject);
                newExam.Type = randomObject.Next(2)+1; // 1 or 2
                examList.Add(newExam);
            }
            lastEndDate = endDate; // for 2nd generation
        }

        // the examNewList should be other than the list from 1st generation
        public static void Generate2(List<Exam> examNewList, DateTime endDate, int count)
        {
            var randomObject = new Random(Guid.NewGuid().GetHashCode());
            for (int i = 0; i < count; i++)
            {
                Exam newExam = new Exam();
                newExam.ID_exam = Interlocked.Increment(ref ID_counter);
                newExam.Date = Generator.Date.GenerateDate(lastEndDate, endDate, randomObject);
                newExam.Type = randomObject.Next(2) + 1;
                examNewList.Add(newExam);
            }
        }

        static public void saveToFile(List<Exam> examList, char separator, string path, bool append)
        {
            string[] strArray = new string[examList.Count];
            for (int i = 0; i < examList.Count; i++)
            {
                strArray[i] = examList.ElementAt(i).ID_exam.ToString()
                            + separator
                            + examList.ElementAt(i).Date.ToShortDateString()
                            + separator
                            + examList.ElementAt(i).Type.ToString();
            }

            if (append)
                UniversalFunctions.appendToFile(strArray, path);
            else
                UniversalFunctions.writeToFile(strArray, path);
        }

    }
}
