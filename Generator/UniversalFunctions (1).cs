﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generator
{
    static class UniversalFunctions
    {
        public static T RandomElement<T>(this IEnumerable<T> source,
                                 Random rng)
        {
            // Optimize for the "known count" case.
            ICollection<T> collection = source as ICollection<T>;
            if (collection != null)
            {
                // ElementAt will optimize further for the IList<T> case
                return source.ElementAt(rng.Next(collection.Count));
            }

            T current = default(T);
            int count = 0;
            foreach (T element in source)
            {
                count++;
                if (rng.Next(count) == 0)
                {
                    current = element;
                }
            }
            if (count == 0)
            {
                throw new InvalidOperationException("Sequence was empty");
            }
            return current;
        }
    }
}
