﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generator
{
    class Lector : Person
    {
        public int Educational { get; private set; }
        public int OralExamResult { get; private set; }
        public int WriteExamResult { get; private set; }
        public int TeachExamResult { get; private set; }
        public Lector Supervisor { get; private set; }

        private static String[] educationaOptions = { "ERROR", "FCE", "FCE/CAE" }; // 1 = "FCE", 2="CAE"

        private void generateEducational(Random rnd)
        {
            lock (lockObject)
            {
                if (!isFirstGenerating)
                    Educational = rnd.Next(2) + 1; // select random educational (1 or 2)
                else
                {
                    Educational = 2; // always someone must can teach CAE
                    isFirstGenerating = false;
                }
            }
        }

        public static new void GenerateUniquePeople<T>(ConcurrentDictionary<string, T> people, ConcurrentDictionary<string, byte> generatedPeople, DateTime now, int numberOfPeople)
            where T: Person, new()
        {
            var tmpPerson = new ConcurrentDictionary<string, T>();
            Person.GenerateUniquePeople<T>(tmpPerson, generatedPeople, now, numberOfPeople);

            var lectors = tmpPerson.Values.ToList() as List<Lector>;
            var rnd = new Random(Guid.NewGuid().GetHashCode());

            foreach (var lector in lectors)
            {
                lector.GenerateSupervisor(lectors, rnd);
                people.GetOrAdd(lector.Pesel, lector as T);
            }
        }

        public static new void GenerateUniquePeople2<T>(ConcurrentDictionary<string, T> people2, ConcurrentDictionary<string, T> people1, ConcurrentDictionary<string, byte> generatedPeople, DateTime now, int numberOfNewPeople)
            where T: Person, new()
        {
            var tmpPerson = new ConcurrentDictionary<string, T>();
            Person.GenerateUniquePeople2<T>(tmpPerson, people1, generatedPeople, now, numberOfNewPeople);

            var lectors = tmpPerson.Values.ToList() as List<Lector>;
            var rnd = new Random(Guid.NewGuid().GetHashCode());

            foreach (var lector in lectors)
            {
                lector.GenerateSupervisor(lectors, rnd);
                people2.GetOrAdd(lector.Pesel, lector as T);
            }
        }

        public override void Generate(DateTime now, Random rnd)
        {
            base.Generate(now, rnd);
            generateEducational(rnd);

            OralExamResult = rnd.Next(70, 101);
            WriteExamResult = rnd.Next(70, 101);
            TeachExamResult = rnd.Next(70, 101);
        }

        public override void Generate2(DateTime now, Random rnd)
        {
            base.Generate2(now, rnd);

            // there is chance for changing educational
            var chance = 0.2; // percentage
            if (Educational == 1 // FCE
                && rnd.NextDouble() < chance)
            {
                Educational = 2; // change on the better
            }

        }

        public void GenerateSupervisor(List<Lector> lectors, Random rnd)
        {
            Lector rndLector;
            int i = 0;
            do {
                rndLector = lectors[rnd.Next(lectors.Count)];
                ++i;
                if (i > 10000)
                    throw new Exception("fatal error");
            } while(rndLector.Supervisor == this);

            Supervisor = rndLector;
        }
        public override String[] getDataArray()
        {
            return new[] { 
                Pesel, 
                Name,
                Surname,
            };
        }

        public override String[] getExcelDataArray()
        {
            return new[]
                {
                    Pesel,
                    Name,
                    Surname,
                    Address,
                    CityCode,
                    City,
                    PhoneNumber.ToString(),
                    OralExamResult.ToString(),
                    WriteExamResult.ToString(),
                    TeachExamResult.ToString(),
                    educationaOptions[Educational],
                    Age.ToString()
                };
        }


    }
}
