﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Generator
{
    class Presence
    {

        //public int ID_lesson { get; set; } //TODO activities random
        public Lesson Lesson { get; private set; }

        public int ID_participation { get; private set; }


        public int TestMark;
        public int ActivePoints; //check if student participates in course

        public override int GetHashCode()
        {
            unchecked 
            {
                int hash = 1009;
                const int factor = 9176;
                hash = (hash * factor) + Lesson.ID_lesson.GetHashCode();
                hash = (hash * factor) + ID_participation.GetHashCode();
                return hash;
            }
        }

        public override bool Equals(object obj)
        {
            var x = obj as Presence;
            if (x == null) throw new Exception("not implement");
            bool equal = x.ID_participation == ID_participation && x.Lesson.ID_lesson == Lesson.ID_lesson;
            return equal;
        }

        public static void Generate(List<Presence> presenceList, List<Lesson>lessonList, List<Participation> participationList, int count)
        {
            // the first assumption
            if ((long)lessonList.Count * participationList.Count < count)
                throw new Exception("too little participations and lessons to generate `count` presence");            

            //////////// CREATE DICTIONARY lesson_ID => List<Participation /////////////////
            // struct needed to search participations relevant to courses
            var courseDictionary = participationList
                .AsParallel().GroupBy(k => k.Course.ID_course, k => k)
                .AsParallel().ToDictionary(k => k.Key, k => k.ToList());
            var checkCounter = courseDictionary
                .AsParallel().Select(x => x.Value.Count)
                .Sum();
            // struct needed to search participations relevant to lessons
            var lessonDictionary = lessonList
                .AsParallel().Join(
                    courseDictionary.AsParallel(), // it is map on participation list
                    x => x.Course.ID_course,
                    x => x.Key,
                    (x, y) => new KeyValuePair<Lesson, List<Participation>>(x, y.Value.ToList()))
                .AsParallel().ToDictionary(x => x.Key, y => y.Value);
            /////////////////////////////////////////////////////////////////////////////////
            //var particLists = lessonDictionary.Values.ToList();
            //var suma = new List<Participation>();
            //foreach (var a in particLists)
            //{
            //    suma.AddRange(a);
            //}
            //suma.Join(courseDictionary, x => x.)


            // to check if it is possible to generate `count` presence
            var maxPresenceNumber = lessonDictionary
                .AsParallel().Select(x => x.Value.Count)
                .AsParallel().Sum();
            if (maxPresenceNumber < count)
                throw new Exception("too little participations and lessons to generate `count` presence");
            
            // copy of dictionary Keys - to improve performance
            var lessonDictionaryKeys = lessonDictionary.Keys.ToList();

            // mix all participationLists - it's optional
            Parallel.ForEach(lessonDictionary, x =>
            {
                var rnd = new Random(Guid.NewGuid().GetHashCode());
                UniversalFunctions.Shuffle(x.Value, rnd);
            });

            // dictionary to find already existing Presence
            HashSet<Presence> presenceDictionary = new HashSet<Presence>(presenceList);
            presenceList.Clear();

            var rnd2 = new Random(Guid.NewGuid().GetHashCode());
            for (int i = 0, j = 0; i < count; ++i) // j is iterator which support lesson selection
            {
                Presence newPresence = new Presence();

                int g = 0;
                bool isOK = false;
                                
                List<Participation> relevatParticipationList;
                do
                {
                    ++j; // always there is other lesson

                    // TODO remove exception - in new logic it won't be thrown
                    if (lessonDictionary.Count == 0)
                        throw new Exception("wrong amount of presence");

                    // lessonDictionary -> relevatParticipationList
                    setLessonAndParticipation(ref j, lessonDictionaryKeys, lessonDictionary, rnd2, newPresence, out relevatParticipationList);

                    // get first of relevant and if it exists, remove it
                    foreach (var partic in relevatParticipationList)
                    {
                        newPresence.ID_participation = partic.ID_participation;
                        if (presenceDictionary.Contains(newPresence) == false)
                        {
                            relevatParticipationList.Remove(partic);
                            isOK = true;
                            break;
                        }
                    }

                    // TODO to improve performance
                    // the partic which already exists (in case 2nd generation) 
                    // should be removed = every elements which this loop check could be removet
                    if (isOK == false)
                    {
                        lessonDictionary.Remove(newPresence.Lesson);
                        lessonDictionaryKeys.RemoveAt(j);
                    }
                    // empty = already used relevation = it could be removed
                    else if (relevatParticipationList.Count == 0)
                    {
                        lessonDictionary.Remove(newPresence.Lesson);
                        lessonDictionaryKeys.RemoveAt(j);
                    }

                    g++;
                    if (g>10000)
                        throw new Exception("nie jest mozliwa poprawna realizacja presence");
                } while (isOK == false);

                // other variables
                newPresence.TestMark = testMarkGenerator(rnd2);
                newPresence.ActivePoints = activePointsRandomGenerator(rnd2);

                presenceDictionary.Add(newPresence);
                //concurrentBag.Add(newPresence);
                //presenceList.Add(newPresence);
            }
            presenceList.AddRange(presenceDictionary);
            //presenceList.AddRange(concurrentBag);
        }

        private static void setLessonAndParticipation(ref int i, List<Lesson> lessonsDictKeys,
            Dictionary<Lesson, List<Participation>> lessonsDictionary, Random rnd, 
            Presence newPresence, out List<Participation> relevatParticipationList)
        {
            ///////// SEARCHNING PARTICIPAT RELEVANT TO LESSONS
            // choosing lesson until find relatives
            // removing lessons without participations
            // it balance amount of presence on lessons by `i` variable
            
            i %= lessonsDictionary.Count; // it could be changed
            var lesson = lessonsDictKeys[i];
            relevatParticipationList = lessonsDictionary[lesson];

            // saving results
            newPresence.Lesson = lesson;
        }

        public static void Generate2(List<Presence> presenceList2, List<Lesson>lessonList2, List<Participation> participationList1and2, int count)
        {
            Generate(presenceList2, lessonList2, participationList1and2, count);
        }

        #region other

        private static Lesson lessonGenerator(List<Lesson> lessonList, Random randomObject)
        {         
            return lessonList[randomObject.Next(lessonList.Count)];
        }
        private static int activePointsRandomGenerator(Random randomObject)
        {
            return  randomObject.Next(5) + 1;
        }
        private static int testMarkGenerator(Random randomObject)
        {
            return randomObject.Next(101);
        }

        public int getTestMark()
        {
            return TestMark;
        }
        public void setTestMark(int mark)
        {
            if (mark >= 0 && mark <= 100)
                TestMark = mark;
            else throw new ArgumentOutOfRangeException();
        }
        public int getActivePoints()
        {
            return ActivePoints;
        }
        public void setActivePoints(int points)
        {
            if (points >= 0 && points <= 5)
                ActivePoints = points;
            else throw new ArgumentOutOfRangeException();
        }



        #endregion
        #region saving

        static public void saveToFile(List<Presence> list, char separator, string path, bool append)
        {
            string[] strArray = new string[list.Count];
            Parallel.For(0, list.Count, i =>
            {
                var presence = list[i];
                strArray[i] = presence.ID_participation.ToString()
                      + separator
                      + presence.Lesson.ID_lesson.ToString()
                      + separator
                      + presence.TestMark
                      + separator 
                      + presence.ActivePoints;
            });

            if (append)
                UniversalFunctions.appendToFile(strArray, path);
            else
                UniversalFunctions.writeToFile(strArray, path);
        }

        #endregion
    }
}
