﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Windows.Forms;
using System.Threading;

namespace Generator
{
    class Lesson
    {
        public int ID_lesson { get; private set; }
        //public string Subject { get; private set; }


        private Subject subject;
        public String Subject
        {
            get
            {
                return subject.getSubject();
            }
        } // subject must be set to get Subject

        public DateTime Date { get; private set; }
        public Lector Lector { get; private set; }
        //public int FK_ID_course { get; private set; }
        public Course Course { get; private set; }

        static private DateTime lastDate; // used in 2nd generation

        private static int ID_counter = 0;

        public Lesson()
        {
        }


        public override int GetHashCode()
        {
            return ID_lesson.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var x = obj as Lesson;
            if (x == null) throw new Exception("not implement");
            return ID_lesson == x.ID_lesson;
        }

        public void Generate(Random rnd, List<Lector> lectors, ShuffleList<Course> courses, DateTime now)
        {          
            ID_lesson = Interlocked.Increment(ref ID_counter);
            subject = new Subject(rnd);

            // DATE GENERATING
            DateTime lastDate;
            int i = 0;
            do
            {
                //Course = courses[rnd.Next(courses.Count)];
                Course = courses.Next();
                lastDate = (now < Course.EndDate) ? now : Course.EndDate;
                ++i;
                if (i > 10000)
                    throw new Exception("petla nieskonczona");

            } while (lastDate < Course.StartDate);            // select the course while the data range isn't ok

            // LECTOR GENERATING
            int j = 0;
            do
            {
                Lector = lectors[rnd.Next(lectors.Count)];
                ++j;
                if (j > 10000)
                    throw new Exception("petla nieskonczona");

            } while (Course.Exam.Type > Lector.Educational);   // if lector dosen't have competence
            
            Date = Generator.Date.GenerateDate(Course.StartDate, lastDate);
        }

        // it bases on the LastDate in course
        public void Generate2(Random rnd, List<Lector> lectors, ShuffleList<Course> courses1and2, DateTime now)
        {
            if (lastDate == null)
                throw new Exception("Bad usage. First use Generate function");

            ID_lesson = Interlocked.Increment(ref ID_counter);
            subject = new Subject(rnd); 
            Lector = lectors[rnd.Next(lectors.Count)];            

            // DATE GENERATING
            // first date must be StartDate of the course or lastDate from previous generating function
            int i = 0;
            DateTime firstDate;
            do
            {
                //Course = courses1and2[rnd.Next(courses1and2.Count)];
                Course = courses1and2.Next();
                firstDate = (lastDate > Course.StartDate) ? lastDate : Course.StartDate;
                ++i;
                if (i > 10000)
                    throw new Exception("petla nieskonczona");

            } while (firstDate > Course.EndDate);            // select the course while the data range isn't ok

            // LECTOR GENERATING
            int j = 0;
            do
            {
                Lector = lectors[rnd.Next(lectors.Count)];
                ++j;
                if (j > 10000)
                    throw new Exception("petla nieskonczona");

            } while (Course.Exam.Type > Lector.Educational);   // if lector dosen't have competence


            Date = Generator.Date.GenerateDate(firstDate, now);  
        }

        public static void Generate(List<Lesson> lessonList, ConcurrentDictionary<string, Lector> lectorsDict, List<Course> courses, DateTime now, int count)
        {
            if (count < courses.Count)
                throw new Exception("wrong number of lessonList - it may result in problems in presence");

            var rnd = new Random(Guid.NewGuid().GetHashCode());
            var shuffleCourses = new ShuffleList<Course>(courses);
            List<Lector> lectors = lectorsDict.Values.ToList();
            for (int i = 0; i < count; i++)
            {
                Lesson newLesson = new Lesson();
                newLesson.Generate(rnd, lectors, shuffleCourses, now);
                lessonList.Add(newLesson);
            }
            lastDate = now;

            // check operations
            var checkSum = lessonList
                .AsParallel().Select(x => x.Course.ID_course)
                .AsParallel().Distinct()
                .AsParallel().Count();
            if (checkSum < courses.Count)
                throw new Exception("there something went wrong");
        }

        public static void Generate2(List<Lesson> lessonList2, ConcurrentDictionary<string, Lector> lectors1, ConcurrentDictionary<string, Lector> lectors2, List<Course> courses1, List<Course> courses2, DateTime now, int count)
        {
            if (count < courses2.Count)
                throw new Exception("wrong number of lessonList - it may result in problems in presence");

            // merging lectors
            List<Lector> lectors = lectors1.Values.Concat(lectors2.Values).ToList();
            var rnd = new Random(Guid.NewGuid().GetHashCode());
            
            var shuffleCourses = new ShuffleList<Course>(courses2);

            for (int i = 0; i < courses2.Count; i++)
            {
                Lesson newLesson = new Lesson();
                newLesson.Generate2(rnd, lectors, shuffleCourses, now);
                lessonList2.Add(newLesson);
            }
            
            // merging courses
            List<Course> courses = courses1.Concat(courses2).ToList();
            shuffleCourses = new ShuffleList<Course>(courses);
            count -= courses2.Count;
            for (int i = 0; i < count; i++)
            {
                Lesson newLesson = new Lesson();
                newLesson.Generate2(rnd, lectors, shuffleCourses, now);
                lessonList2.Add(newLesson);
            }


            // check operations
            var checkSum = lessonList2
                .AsParallel().Select(x => x.Course.ID_course)
                .AsParallel().Distinct()
                .AsParallel().Count();
            if (checkSum < courses2.Count)
                throw new Exception("there something went wrong");
        }


        private String[] getDataArray()
        {
            return new[] { 
                ID_lesson.ToString(), 
                Subject, 
                Date.ToShortDateString(),
                Lector.Pesel,
                Course.ID_course.ToString()
            };
        }

        public static void SaveToFile(List<Lesson> LessonList, char separator, string path, bool append)
        {
            string[] strArray = new string[LessonList.Count];
            Parallel.For(0, LessonList.Count, i =>
            {
                var dataArray = LessonList[i].getDataArray();
                strArray[i] = ""; // clear the string
                foreach (var g in dataArray)
                    strArray[i] += g + separator;
                strArray[i] = strArray[i].Remove(strArray[i].Count() - 1); // remove last separator
            });

            if (append)
                UniversalFunctions.appendToFile(strArray, path);
            else
                UniversalFunctions.writeToFile(strArray, path);
        }

        static public void writeToFile(string[] strArray, string path)
        {
            var encoding = System.Text.Encoding.GetEncoding("ISO-8859-2");
            using (StreamWriter outputFile = new StreamWriter(path, false))
            {
                foreach (var element in strArray)
                {
                    outputFile.WriteLine(element, encoding);
                }
            }
            Cursor.Current = Cursors.Default;
        }
    }
}
