﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace Generator
{
    public partial class Form1 : Form
    {
        #region variables
        private List<Exam> ExamList;
        private List<Exam> ExamList2;
        private List<Lesson> LessonList;
        private List<Lesson> LessonList2;
        private List<Course> CourseList;
        private List<Course> CourseList2;
        private List<Presence> PresenceList;
        private List<Presence> PresenceList2;
        private List<Participation> ParticipationList;
        private List<Participation> ParticipationList2;

        private FolderBrowserDialog saveFileDialog;

        private string pathStudents;
        private string pathLectors;
        private string pathExams;
        private string pathCourses;
        private string pathLessons;
        private string pathPresence;
        private string pathParticipation;
        private int count;

        private ConcurrentDictionary<string, Lector> lectors;
        private ConcurrentDictionary<string, Lector> lectors2;
        private ConcurrentDictionary<string, Student> students;
        private ConcurrentDictionary<string, Student> students2;
        private ConcurrentDictionary<string, byte> generatedPeople; // value is't needed
        private DateTime date;
        private DateTime newDate;
        private int studentCount2;
        private int lectorsCount2;
        private int examCount2;
        private int courseCount2;
        private int lessonCount2;
        private int participationCount2;
        private int studentCount;
        private int lectorsCount;
        private int examCount;
        private int courseCount;
        private int lessonCount;
        private int participationCount;
        private int presenceCount;
        private int presenceCount2;
        private int participationGroup;
        private int participationGroup2;

        private bool wasGenerated;
        private string pathMain;
        private string metaDataFileName;

        #endregion
        #region initialization
        public Form1()
        {
            InitializeComponent();
            lectors = new ConcurrentDictionary<string, Lector>();
            lectors2 = new ConcurrentDictionary<string, Lector>();
            students = new ConcurrentDictionary<string, Student>();
            students2 = new ConcurrentDictionary<string, Student>();
            generatedPeople = new ConcurrentDictionary<string, byte>();

            ExamList = new List<Exam>();
            ExamList2 = new List<Exam>();
            LessonList = new List<Lesson>();
            LessonList2 = new List<Lesson>();
            CourseList = new List<Course>();
            CourseList2 = new List<Course>();
            PresenceList = new List<Presence>();
            PresenceList2 = new List<Presence>();
            ParticipationList = new List<Participation>();
            ParticipationList2 = new List<Participation>();
            saveFileDialog = new FolderBrowserDialog();
            wasGenerated = false;

            //time-changing filename pattern used to allow multiple file version testing
            //metaDataFileName = DateTime.Now.ToLongTimeString().Replace(':', '.') + ".txt";
            metaDataFileName =".txt";
            pathMain = "D:\\HD\\";
            pathStudents = pathMain + "generatedStudents_" + metaDataFileName;
            pathLectors = pathMain + "generatedLectors_" + metaDataFileName;
            pathExams = pathMain + "generatedExams_" + metaDataFileName;
            pathCourses = pathMain + "generatedCourses_" + metaDataFileName;
            pathLessons = pathMain + "generatedLessons_" + metaDataFileName;
            pathParticipation = pathMain + "generatedParticipations_" + metaDataFileName;
           
            Time2Box.Enabled = false;
            //Time1Box.Value = Time2Box.Value.AddYears(-1); //TODO Solve problem with date
            //pathBox.Visible = false;
            pathBox.Text = pathMain;
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            progressBar1.Value = 0;
            //generateButton.Enabled = false;

            date = Time1Box.Value;
            Time2Box.Value = new DateTime(date.Year+1,date.Month,date.Day);
            newDate = Time2Box.Value; // TODO date from GUI
        }


        #endregion
        #region logic

        private void generateButton_Click(object sender, EventArgs e)
        {
            if(!boolTime2.Checked) ResetButton_Click(sender,e);
            date = Time1Box.Value;
            newDate = Time2Box.Value;
            //metaDataFileName = DateTime.Now.ToLongTimeString().Replace(':', '.') + ".txt";
            pathStudents = pathMain + "generatedStudents_" + metaDataFileName;
            pathLectors = pathMain + "generatedLectors_" + metaDataFileName;
            pathExams = pathMain + "generatedExams_" + metaDataFileName;
            pathCourses = pathMain + "generatedCourses_" + metaDataFileName;
            pathLessons = pathMain + "generatedLessons_" + metaDataFileName;
            pathParticipation = pathMain + "generatedParticipations_" + metaDataFileName;
            pathPresence = pathMain + "generatedPresence_" + metaDataFileName;

            studentCount = (int)StudentBox.Value;
            lectorsCount = (int)LectorsBox.Value;
            examCount = (int)ExamsBox.Value;
            courseCount = (int)CoursesBox.Value;
            lessonCount = (int)ActivitiesBox.Value;
            participationCount = (int)ParticipationsBox.Value-studentCount;
            presenceCount = (int)PresenceBox.Value;
            participationGroup = 5;

            generateButton.Enabled = false;

            if (!wasGenerated)
            {
                //reading values from GUI - 1st generation
                generateButton.Enabled = false;

                Person.GenerateUniquePeople(students, generatedPeople, studentCount);
                Student.saveToFile(students, '|', pathStudents, false);
                progressBar1.Value = 20;

                Person.GenerateUniquePeople(lectors, generatedPeople, lectorsCount);
                Person.saveToFile(lectors, '|', pathLectors, false);
                progressBar1.Value = 40;

                Exam.Generate(ExamList, new DateTime(date.Year-4, 1, 1), new DateTime(date.Year + 1,1,1), examCount);
                Exam.saveToFile(ExamList, '|', pathExams, false);
                progressBar1.Value = 50;

                Course.Generate(CourseList, ExamList, courseCount);
                Course.saveToFile(CourseList, '|', pathCourses, false);
                progressBar1.Value = 60;

                Lesson.Generate(LessonList, lectors, CourseList, date, lessonCount);
                Lesson.SaveToFile(LessonList, '|', pathLessons, false);
                progressBar1.Value = 70;

                Participation.Generate(ParticipationList, CourseList, students.Values.ToList(), 
                    Time1Box.Value, participationGroup,participationCount);
                Participation.saveToFile(ParticipationList, '|', pathParticipation, false);
                progressBar1.Value = 80;

                Presence.Generate(PresenceList, LessonList, ParticipationList, presenceCount);
                Presence.saveToFile(PresenceList, '|', pathPresence, false);
                progressBar1.Value = 90;

                //**********************************************//
                //****************  EXCEL PART 1 ***************//
                //**********************************************//
                Lector.saveToExcel(lectors,',', pathLectors, false);
                Student.saveToExcel(students,ParticipationList, ',', pathStudents, false);
                progressBar1.Value = 100;
                //**********************************************//
                wasGenerated = true;
                generateButton.Enabled = true;

            }
            
            if (boolTime2.Checked) generateButton2_Click();

            if (pathMain == "") Process.Start(AppDomain.CurrentDomain.BaseDirectory);
            else Process.Start(pathMain);
            generateButton.Enabled = false;
        }

        private void generateButton2_Click()
        {
            //setting values from GUI - 2nd generation
            var multiple = (double)time2IncreaseDataPercentage.Value / 100.0;
            studentCount2 = (int)(studentCount * multiple);
            lectorsCount2 = (int)(lectorsCount * multiple);
            examCount2 = (int)(examCount * multiple);
            courseCount2 = (int)(courseCount * multiple);
            lessonCount2 = (int)(lessonCount * multiple);
            participationCount2 = (int)(participationCount * multiple);
            presenceCount2 = (int)(presenceCount * multiple);
            participationGroup2 = participationGroup;

            // other settings
            date = Time1Box.Value;
            newDate = Time2Box.Value;
            ResetButton.Enabled = false;
            var addition = "2_"; // addition to default path
            //metaDataFileName = DateTime.Now.ToLongTimeString().Replace(':', '.') + ".txt";
            pathStudents = pathMain + addition + "generatedStudents_" + metaDataFileName;
            pathLectors = pathMain + addition + "generatedLectors_" + metaDataFileName;
            pathExams = pathMain + addition + "generatedExams_" + metaDataFileName;
            pathCourses = pathMain + addition + "generatedCourses_" + metaDataFileName;
            pathLessons = pathMain + addition + "generatedLessons_" + metaDataFileName;
            pathParticipation = pathMain + addition + "generatedParticipations_" + metaDataFileName;
            pathPresence = pathMain + addition + "generatedPresence_" + metaDataFileName;

            progressBar1.Value = 0;
            Person.GenerateUniquePeople2(students2, students, generatedPeople, studentCount2);
            Student.saveToFile(students, '|', pathStudents, false);
            Student.saveToFile(students2, '|', pathStudents, true);

            progressBar1.Value = 20;

            Person.GenerateUniquePeople2(lectors2, lectors, generatedPeople, lectorsCount2);
            Person.saveToFile(lectors, '|', pathLectors, false);
            Person.saveToFile(lectors2, '|', pathLectors, true);
            progressBar1.Value = 40;

            Exam.Generate2(ExamList2, new DateTime(newDate.Year + 1, 1, 1), examCount2);
            Exam.saveToFile(ExamList, '|', pathExams, false);
            Exam.saveToFile(ExamList2, '|', pathExams, true);
            progressBar1.Value = 50;

            Course.Generate(CourseList2, ExamList2, courseCount2);
            Course.saveToFile(CourseList, '|', pathCourses, false);
            Course.saveToFile(CourseList2, '|', pathCourses, true);
            progressBar1.Value = 60;

            Lesson.Generate2(LessonList2, lectors, lectors2, CourseList, CourseList2, newDate, lessonCount2);
            Lesson.SaveToFile(LessonList, '|', pathLessons, false);
            Lesson.SaveToFile(LessonList2, '|', pathLessons, true);
            progressBar1.Value = 70;

            Participation.Generate2(ParticipationList2, ParticipationList, CourseList2, students.Values.Concat(students2.Values).ToList(), 
                students2.Values.ToList(), newDate, participationGroup2, participationCount2);
            Participation.saveToFile(ParticipationList, '|', pathParticipation, false);
            Participation.saveToFile(ParticipationList2, '|', pathParticipation, true);
            progressBar1.Value = 80;

            Presence.Generate2(PresenceList2, LessonList2, ParticipationList.Concat(ParticipationList2).ToList(), presenceCount2);
            Presence.saveToFile(PresenceList, '|', pathPresence, false);
            Presence.saveToFile(PresenceList2, '|', pathPresence, false);
            progressBar1.Value = 90;
            //**********************************************//
            //****************  EXCEL PART  ****************//
            //**********************************************//
            Lector.saveToExcel(lectors2, ',', pathLectors, false);
            Student.saveToExcel(students2, ParticipationList.Concat(ParticipationList2).ToList(), ',', pathStudents, false);
            //**********************************************//
            progressBar1.Value = 100;

            generateButton.Enabled = false;
            ResetButton.Enabled = true;
            ResetButton.ForeColor = Color.ForestGreen;
        }


        #endregion
        #region other

        private void label1_Click(object sender, EventArgs e)
        {
        }
        private void label3_Click(object sender, EventArgs e)
        {
        }

        private void peselBox_TextChanged(object sender, EventArgs e)
        {
        }

        private void localizeButton_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                pathMain = saveFileDialog.SelectedPath+"\\";
                metaDataFileName = DateTime.Now.ToLongTimeString().Replace(':', '.') + ".txt";
                pathStudents = pathMain + "generatedStudents_" + metaDataFileName;
                pathLectors = pathMain + "generatedLectors_" + metaDataFileName;
                pathExams = pathMain + "generatedExams_" + metaDataFileName;
                pathCourses = pathMain + "generatedCourses_" + metaDataFileName;
                pathLessons = pathMain + "generatedLessons_" + metaDataFileName;
                pathParticipation = pathMain + "generatedParticipations_" + metaDataFileName;
                generateButton.Enabled = true;
                pathBox.Text = pathMain;
                pathBox.Visible = true;
                //generateButton.BackColor = Color.GreenYellow;
                //localizeButton.Enabled = false;

            }
            
        }

        //private void countBox_TextChanged(object sender, EventArgs e)
        //{
        //    int a;
        //    bool success = Int32.TryParse(countBox.Text, out a);
        //    if (success == true && a < trackBar1.Maximum)
        //    {
        //        trackBar1.Value = a;
        //        count = a;
        //    }
        //}
        //private void trackBar1_Scroll(object sender, EventArgs e)
        //{
        //    trackBar1.Value = count;
        //    countBox.Text = trackBar1.Value.ToString();
        //}

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            if(StudentBox.Value == 1000000)
                StudentBox.BackColor = Color.LightCoral;
            else
            {
                StudentBox.BackColor = Color.White;
            }
            if (ParticipationsBox.Value < StudentBox.Value)
                ParticipationsBox.Value = StudentBox.Value;
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void boolTime1_CheckedChanged(object sender, EventArgs e)
        {
            boolTime1.Checked = true;
        }

        private void boolTime2_CheckedChanged(object sender, EventArgs e)
        {
            Time2Box.Enabled = boolTime2.Checked;
            generateButton.Enabled = true;
        }

        

        private void ResetButton_Click(object sender, EventArgs e)
        {
            lectors = new ConcurrentDictionary<string, Lector>();
            lectors2 = new ConcurrentDictionary<string, Lector>();
            students = new ConcurrentDictionary<string, Student>();
            students2 = new ConcurrentDictionary<string, Student>();
            generatedPeople = new ConcurrentDictionary<string, byte>();
            

            ExamList = new List<Exam>();
            ExamList2 = new List<Exam>();
            LessonList = new List<Lesson>();
            LessonList2 = new List<Lesson>();
            CourseList = new List<Course>();
            CourseList2 = new List<Course>();
            ParticipationList = new List<Participation>();
            ParticipationList2 = new List<Participation>();
            PresenceList = new List<Presence>();
            PresenceList2 = new List<Presence>();

            saveFileDialog = new FolderBrowserDialog();
            wasGenerated = false;
            progressBar1.Value = 0;
            generateButton.Enabled = true;
            ResetButton.ForeColor = Color.Black;
        }

        private void LectorsBox_ValueChanged(object sender, EventArgs e)
        {
            if (LectorsBox.Value == 1000000)
                LectorsBox.BackColor = Color.LightCoral;
            else
            {
                LectorsBox.BackColor = Color.White;
            }
        }

        private void ActivitiesBox_ValueChanged(object sender, EventArgs e)
        {
            if (ActivitiesBox.Value == 1000000)
                ActivitiesBox.BackColor = Color.LightCoral;
            else
            {
                ActivitiesBox.BackColor = Color.White;
            }
        }

        private void ParticipationsBox_ValueChanged(object sender, EventArgs e)
        {
            if (ParticipationsBox.Value == 1000000)
                ParticipationsBox.BackColor = Color.LightCoral;
            else
            {
                ParticipationsBox.BackColor = Color.White;
            }
            if (ParticipationsBox.Value < StudentBox.Value)
                ParticipationsBox.Value = StudentBox.Value;
        }

        private void PresenceBox_ValueChanged(object sender, EventArgs e)
        {
            if (PresenceBox.Value == 1000000)
                PresenceBox.BackColor = Color.LightCoral;
            else
            {
                PresenceBox.BackColor = Color.White;
            }
        }

        private void CoursesBox_ValueChanged(object sender, EventArgs e)
        {
            if (CoursesBox.Value == 1000000)
                CoursesBox.BackColor = Color.LightCoral;
            else
            {
                CoursesBox.BackColor = Color.White;
            }
        }

        private void ExamsBox_ValueChanged(object sender, EventArgs e)
        {
            if (ExamsBox.Value == 1000000)
                ExamsBox.BackColor = Color.LightCoral;
            else
            {
                ExamsBox.BackColor = Color.White;
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void Time1Box_ValueChanged(object sender, EventArgs e)
        {
            if (Time1Box.Value >= Time2Box.Value) 
                Time1Box.Value = new DateTime(Time2Box.Value.Year - 1, Time2Box.Value.Month, Time2Box.Value.Day);
            date = Time1Box.Value;
            newDate = Time2Box.Value;
        }

        private void Time2Box_ValueChanged(object sender, EventArgs e)
        {

            if (Time1Box.Value >= Time2Box.Value) 
                Time1Box.Value = new DateTime(Time2Box.Value.Year - 1, Time2Box.Value.Month, Time2Box.Value.Day);
            date = Time1Box.Value;
            newDate = Time2Box.Value;
        }

        private void settingsButton1_Click(object sender, EventArgs e)
        {

        }

        private void settingsButton2_Click(object sender, EventArgs e)
        {

        }

        private void settingsButton3_Click(object sender, EventArgs e)
        {

        }

        private void settingsButton4_Click(object sender, EventArgs e)
        {

        }

      
    }
}
        #endregion