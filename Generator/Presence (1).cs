﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Generator
{
    class Presence
    {
        public int ID_activities { get; set; } //TODO activities random
        public int ID_participation { get; set; }
        public int TestMark;
        public int ActivePoints; //check if student participates in course
       
        public int getTestMark()
        {
            return TestMark;
        }
        public void setTestMark(int mark)
        {
            if (mark >= 0 && mark <= 100)
                TestMark = mark;
            else throw new ArgumentOutOfRangeException(); 
        }
        public int getActivePoints()
        {
            return ActivePoints;
        }
        public void setActivePoints(int points)
        {
            if (points >= 0 && points <= 5)
                ActivePoints = points;
            else throw new ArgumentOutOfRangeException(); 
        }
        public static void Generate(List<Presence> presenceList, List<Participation> participationList, int count)
        {
            for (int i = 0; i < count; i++)
            {
                Presence NewPresence = new Presence();
                NewPresence.ID_participation = participationGenerator(participationList);
                NewPresence.TestMark = testMarkGenerator();
                NewPresence.ActivePoints = activePointsRandomGenerator();
                presenceList.Add(NewPresence);
            }
        }
        private static int participationGenerator(List<Participation> participationList)
        {
            var randomObject = new Random(Guid.NewGuid().GetHashCode());
            return randomObject.Next(participationList.Count);
        }
        private static int activePointsRandomGenerator()
        {
            var randomObject = new Random(Guid.NewGuid().GetHashCode());
            return  randomObject.Next(5) + 1;
        }
        private static int testMarkGenerator()
        {
            var randomObject = new Random(Guid.NewGuid().GetHashCode());
            return randomObject.Next(101);
        }
    }
}
